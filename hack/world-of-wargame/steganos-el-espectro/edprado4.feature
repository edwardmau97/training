Feature: Solve El Espectro Challenge
  from site World of wargame
  logged in as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: First failed attempt
  Given the downloaded audio from the challenge page
  When I play the audio
  And I realize that there is chirp noise added to the original sound
  Then I try to decode it as morse code
  But I dont get any message
  And I fail the challenge

Scenario: Challenge Solved
  Given The challenge presented
  And My experience from the previous attempt
  When I use Audacity to separate the left channel from the audio
  And I crop the audio to the chirp sound
  And I import the cropped sound into a spectrogram analyser
  Then I found a message inside the spectrogram
  And I solve the challenge
