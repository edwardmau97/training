from PIL import Image
from skimage.measure import structural_similarity as ssim
import cv2
from numpy import array

def compare(strA, strB):
    imgA = cv2.imread(strA)
    imgB = cv2.imread(strB)
    imgA = cv2.cvtColor(imgA, cv2.COLOR_BGR2GRAY)
    imgB = cv2.cvtColor(imgB, cv2.COLOR_BGR2GRAY)
    return ssim(imgA,imgB)

marr = ["" for x in range(498)] #se crea un array de string vacio de 498 posiciones
i = 0
while i < 498:  #llenamos el array con los nombres de los recortes
    marr[i] = "img_"+str(i)+".png"
    i+=1
final = Image.new("RGB",(498,130),"green") #crear una imagen vacia de fondo verde para pegar los recortes

p = 0
pos = 0
d = 0
t = 0
j=0
while j<498:
        i = 0
        m = 0
        while i < 498:
           if (marr[i] != "n") and (marr[p] != "n"):
              r = compare("img/"+marr[p],"img/"+marr[i])
              if (r > m) and (r != 1.0):
                  m = r
                  d = p
                  t = i
           i+=1
        imagen = Image.open("img/img_"+str(d)+".png")
        final.paste(imagen,(pos,0))
        p = t
        pos+=1
        marr[d] = "n"
        j+=1
final.show()
final.save("solucion.png")
