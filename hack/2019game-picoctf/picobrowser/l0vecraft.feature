## Version 2.0
## language: en

Feature: Picobrowser - web - picoCTF
  Code:
    Picobrowser
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>              |
    | Ubuntu             | 18.04                  |
    | FireFox            | 72.0.1(64 bits)        |
    | BurpSuite Community| 2.1.07(64 bits)       |
  Machine information:
    Given I access to the challenge via browser
    When I enter to the challen see a big button it's says "Flag"
    Then I click on it
    And a message appears to me "You're not picobrowser!" with my user agent
    And now I can assume the challenge consist in modify the user agent param

  Scenario: Success: Change the user agent param
    When I intercept the request see my actual user agent value
    """
    Mozilla/5.0 (X11; Linux x86_64)...
    """
    Then I change the value to "picobrowser"
    When I forward it
    Then shows me the flag
    And now I can resolve the challenge
