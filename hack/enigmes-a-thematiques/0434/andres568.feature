## Version 2.0
## language: en

Feature: 434-H4CK1NG-enigmes-a-thematiques
  Code:
    434
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
    | Ghidra          | 9.1.2         |
  Machine information:
    Given I am accessing the challenge page

  Scenario: Fail:search-for-keywords
    Given I am on the main page
    And I can see the message
    """
    Voici un deuxième programme sans le code source. Il n'y a que l'exécutable
    et pour accéder à l'application, il faut un mot de passe qui n'est pas
    fourni. Saurez-vous le trouver ?
    """
    When I press the link called "Télécharger l'exécutable"
    Then A file is downloaded
    When I open the file
    Then I see a bunch of characters
    When I search for the keyword "passe"
    Then I find a possible "<FLAG>"
    """
    Entrer le mot de passe : �Ok�dommageee...
    """
    When I submit the possible "<FLAG>" to pass the challenge
    Then The possible "<FLAG>" does not work
    And I can not capture the flag

  Scenario: Success:decompile-file
    Given I am on the main page
    When I download the file
    Then I can see a bunch of characters which start with "ELF"
    When I search for "ELF" on internet
    Then I find that "ELF" is a format for executables
    When I use a reverse engineering tool called Ghidra
    Then I find the related code
    """
    if((((local_1a == <FLAG1/6>) && (local_19 == <FLAG2/6>)) &&
    (local_18 == <FLAG3/6>)) &&(((local_17 == <FLAG4/6> &&
    (local_16 == <FLAG5/6>)) && (local_15 == <FLAG6/6>))))
    {
      FUN_0804838b(&DAT_080485da);
    }
    else {
      FUN_0804838b(0x80485dd);
    }
    """
    When I analyze the code
    Then I can make the "<FLAG>" regarding the related code
    When I submit the "<FLAG>" to pass the challenge
    Then I capture the flag
