## Version 2.0
## language: en

Feature: gremlin-sqli-lord-of-sqlinjection
  Site:
    lord-of-sqlinjection
  User:
    jpverde
  Goal:
    Get access as admin user

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given A webpage
    """
    https://los.rubiya.kr/chall/goblin_e5afb87a6716708e3af46a849517afdc.php
    """
    And The webpage shows the php code with the query function

  Scenario: Success:sql-injection
    Given I am looking at the code shown
    And I can see the resulting query validates the data in 'id' column
    And In the 'no' column, but the 'no' receives an 'int' value
    And Is not sanitized
    When I type this in the url trying to inject sql "?no=1 or 1=1 limit 1,1"
    And To get a true response, so it recognizes it as admin logged
    Then I get this response
    """
    GOBLIN Clear!
    """
    And And I completed the challenge
