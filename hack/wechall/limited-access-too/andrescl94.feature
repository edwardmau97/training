# language: en

Feature: Solve the challenge Limited Access Too
  Identified with code Undefined
  From the site www.wechall.net
  From the category Exploit, HTTP
  With my username AndresCL94

Background:
  Given that I am registered in the website Wechall.net
  And I have solved the previous challenge Limited Access
  And I am running the OS Windows 7 Ultimate, version x64
  And I have Google Chrome 60.0.3112.113 installed as the default web explorer
  And I have access to the internet
  And I have installed the plug-in cookies.txt available for Google Chrome
  And I am logged in the website of the challenge
  And I use the plug-in to save the session information (cookies) to a text file
  And I have installed the software VMware Workstation 12 Player
  And I am running a virtual image of the Linux-OS Kali x64, version 2017.1
  And I copy the file cookies.txt from Windows (Host OS) to Kali (Guest OS)

Scenario: Successful Solution
  Given that I am trying to access a website protected by a .htaccess file
  And I have access to the directives of the .htaccess file
  And I have investigated about the HTTP request commands
  When I see that the file limited most of the HTTP commands but not all of them
  And I notice that PATCH is still available
  Then I run the command wget in Kali
  And I add the options --load-cookies cookies.txt and --method=PATCH
  And I add the URL of the website
  Then I solve the challenge
