/*
Compiling with g++
$ g++ -o kedavamaru kedavamaru.cpp

Lintint nazi with cppcheck
$ cppcheck -I '/usr/include/c++/' --enable=all --inconclusive \
> --force kedavamaru.cpp

Checking kedavamaru.cpp ...
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>

using namespace std;

// Unkown character
char unkwown = char(176);

bool is09(char const &c);
bool isAZ(char const &c);
bool isPN(char const &c);

// character-wise OR between strings
// i.e. entropy merger
string OR(string a, string b) {
  // if b contains information that a don't, merge b into a
  for (unsigned int i = 0; i < a.size(); ++i) {
    bool conda = (a[i] == unkwown);
    bool condb = (b[i] != unkwown);
    a[i] = ( conda && condb ) ? b[i] : a[i];
  }
  return a;
}

// file name formater
string getname(int index) {
  string name = "inputlogged/index (" + to_string(index) + ").php";
  return name;
}

// HTML parser and decypher
string getfromhtml(string filename, ostream &log) {
  ifstream file(filename);

  string cln = "<";
  string raw = "<";

  char c;
  int status = 0;
  for (;;) {
    c = file.get();

    if (c == '>') status = 0;
    if (status == 0 && c == '<') ++status;
    if (status == 1 && c == '!') ++status;
    if (status == 2 && c == '-') ++status;
    if (status == 3 && c == '-') ++status;
    if (status == 4) break;
  }
  while (c != '>') {
    c = file.get();
    if (isAZ(c) || is09(c) || isPN(c)) raw += c;
  }
  raw += '>';

  file.close();

  int i = 0, is = 0;
  while (raw[i] != '>') {
    if (isAZ(raw[i]) || is09(raw[i])) {
      is = 0;
      while (is09(raw[i + 1])) {
        is = 1;
        ++i;
      }
      if (is) cln += unkwown;
      else cln += raw[i];
    }
    else {
      if (isPN(raw[i])) cln += raw[i];
    }
    ++i;
  }
  cln += '>';

  log << cln << endl;

  return cln;
}

int main() {
  ofstream log("log.txt");

  int warnings = 0;
  string msg, code, filename;

  vector<set<char>> answer(2000);

  msg = getfromhtml(getname(1), log);

  for (int i = 2; i <= 997; ++i) {
    filename = getname(i);
    cout << "Processing: " << filename << endl;

    code = getfromhtml(filename, log);

    if (code.size() == msg.size()) {
      msg = OR(msg, code);
      for (int k = 0; k < (int)msg.size(); ++k) {
        if (code[k] != unkwown) answer[k].insert(code[k]);
      }
    }
    else ++warnings;
  }

  cout << "Finished with " << warnings << " warnings" << endl;

  log << msg;

  for (int k = 0; k < (int)msg.size(); ++k) {
    if (answer[k].size() == 0) {
      cout << "_";
    }
    if (answer[k].size() == 1) {
      for (char const &c : answer[k]) cout << c;
    }
    if (answer[k].size() > 1) {
      cout << "(";
      for (char const &c : answer[k]) cout << c;
      cout << ")";
    }
  }

  cout << endl << endl;
  return 0;
}

bool is09(char const &c) {
  switch (c) {
    case '0': return true;
    case '1': return true;
    case '2': return true;
    case '3': return true;
    case '4': return true;
    case '5': return true;
    case '6': return true;
    case '7': return true;
    case '8': return true;
    case '9': return true;
    default: return false;
  }
}
bool isAZ(char const &c) {
  switch (c) {
    case 'a': return true;
    case 'b': return true;
    case 'c': return true;
    case 'd': return true;
    case 'e': return true;
    case 'f': return true;
    case 'g': return true;
    case 'h': return true;
    case 'i': return true;
    case 'j': return true;
    case 'k': return true;
    case 'l': return true;
    case 'm': return true;
    case 'n': return true;
    case 'o': return true;
    case 'p': return true;
    case 'q': return true;
    case 'r': return true;
    case 's': return true;
    case 't': return true;
    case 'u': return true;
    case 'v': return true;
    case 'w': return true;
    case 'x': return true;
    case 'y': return true;
    case 'z': return true;

    case 'A': return true;
    case 'B': return true;
    case 'C': return true;
    case 'D': return true;
    case 'E': return true;
    case 'F': return true;
    case 'G': return true;
    case 'H': return true;
    case 'I': return true;
    case 'J': return true;
    case 'K': return true;
    case 'L': return true;
    case 'M': return true;
    case 'N': return true;
    case 'O': return true;
    case 'P': return true;
    case 'Q': return true;
    case 'R': return true;
    case 'S': return true;
    case 'T': return true;
    case 'U': return true;
    case 'V': return true;
    case 'W': return true;
    case 'X': return true;
    case 'Y': return true;
    case 'Z': return true;
    default: return false;
  }
}
bool isPN(char const &c) {
  switch (c) {
    case ' ': return true;
    case ',': return true;
    case '.': return true;
    case '?': return true;
    case '!': return true;
    case ';': return true;
    case ')': return true;
    case '\'': return true;
    default: return false;
  }
}

/*
Assuming there is a folder named 'inputlogged' with 997 files named
'index (*).php' where * is a number from 1 to 997 whose contents
are HTML snapshots as of (12 october 2018) of
'https://www.wechall.net/en/challenge/Z/EN/index.php' logged in with
the username kedavamaru

$ ./kedavamaru

Processing: inputlogged/index (2).php
Processing: inputlogged/index (3).php
Processing: inputlogged/index (4).php

...

Processing: inputlogged/index (995).php
Processing: inputlogged/index (996).php
Processing: inputlogged/index (997).php
Finished with 0 warnings

< hello nubcake. _he solu__on _o _h_s book c_phe_ _s, of cou_se, f_nd_ng
_he __gh_ way. now you_ flag _s aplpgnm__scm, and now as you found __,
you p_obably enjoy hav_ng c_ea_ed a sys_em _o _ead _h_s.  _sn_ __ a beau_y?
_he elegance of be_ng able _o say wha_ we wan_, and only people cleve_
enough be_ng able _o _ead __? _h_s _em_nds of evolu__on, ma_h and ph_losphy,
doesn'_ __?  howeve_  do no_ fo_ge_ _o keep _he scene al_ve  ;)  hack _he
plane_! g_zmo_ewechalldo_ne_ >
*/
