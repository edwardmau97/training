# language: en

Feature: Solve bill for Bill challenge
  From the WeChall site
  Of the category Cracking and Storyline
  As the registered user disassembly

  Background:
    Given I am running macOS High Sierra 10.13.4 with Homebrew 0.9.9
    And I am using ophcrack 3.6.1
    And I have the XP free small Rainbow tables (SSTIC04-10k)
    And I am using KeePassX 2.0.3

  Scenario: Decoding the hint
    Given a PHP page with the storyline
    And three downloadabled files with encrypted data
    And a hidden hint with the same color as the background
    """
    hidden hint: Sdfr swri cntjh ea rnjuah ws jawrs fm sdqaa hfllaqams vwyr:
    Sda lfqrs wmh oqalaqqah vwy fr sn tra qwfmenv swejar. F rtbbars Nodcqwci
    lnq sdfr cdwjjamba - ets ynt dwua sn hnvmjnwh w ~ 380 KE qwfmenv sweja.
    Sda racnmh vwy fr trfmb rnka eqtsa lnqca wsswci - fs maahr w jns nl COT
    ets fs'r rnjuaweja. Wmh lfmwjjy fl ynt dwua jtci sdaqa wqa rnka nmjfma JK
    cqwcifmb owbar, kwyea sday cwm cqwci fs lnq ynt. Fl ynt dwua mns sqfah
    qwfmenv swejar yas, bfua sdak w sqy :)
    """
    Then I use the online tool 'https://quipqiup.com' to decrypt the message
    """
    hidden hint: This task could be solved at least in three different ways:
    The first and preferred way is to use rainbow tables. I suggest Ophcrack
    for this challenge - but you have to download a ~ 380 MB rainbow table.
    The second way is using some brute force attack - it needs a lot of CPU
    but it's solveable. And finally if you have luck there are some online LM
    cracking pages, maybe they can crack it for you. If you have not tried
    rainbow tables yet, give them a try :)
    """

  Scenario: Crack the SAM file breaking LM hashes
    Given the ophcrack tool and the XP free small Rainbow tables
    And the system and SAM files, I get the following passwords:
    """
    $ ophcrack -d tables_xp_free_small/ -t tables_xp_free_small -n 5 -w files/
    4 hashes have been found in the encrypted SAM found in files/.

    Opened 4 table(s) from tables_xp_free_small.
    0h  0m  0s; Found empty password for user *disabled* Guest (NT hash #1)
    0h  0m  3s; Found password 2U1Z99 for 2nd LM hash #0in table XP free small
                      #3 at column 9753.
    0h  0m  5s; Found password W3CH411 for 1st LM hash #0in table XP free small
                      #2 at column 8529.
    0h  0m  5s; Found password W3cH4112u1Z99 for user Bill (NT hash #0)
    0h  0m  5s; search (100%); tables: total 4, done 0, using 4; pwd found 2/2.

    Results:

    username / hash                  LM password    NT password
    Bill                             W3CH4112U1Z99  W3cH4112u1Z99
    *disabled* Guest                 *** empty ***  *** empty ***
    """

  Scenario: Get the credit card information from the KeePass database
    Given the keepass.kdb file, I use the LM password obtained before
    And import the db V.1 with the KeePassX tool
    Then I get the following entries:
    """
    eMail > My hotmail account
    Username: BillG@h0tma1l.com

    credit card > Amex
    371234567895006:08/05-18/05:562
    """

  Scenario: Submit the information to solve the challenge
    Given the credit card information, I submit the results
    Then shows me that I did a good job but one thing is missing
    And I need to login in the with e-mail account
    Then I access 'https://www.wechall.net/challenge/Z/bill_for_bill/login.php'
    And delete the order notification from the inbox and deleted folders
    Then shows me the solution
    """
    Chall finished, good job.
    The solution for this challenge is passwordsuxx
    """
    Then I enter the keyword in the text box and solve the challenge
