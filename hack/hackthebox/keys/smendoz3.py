#!/usr/bin/env python
# Python smendoz3.py
"""
Decode Fernet symmetric encryption given a Key, and a Token/cipher
"""

from cryptography.fernet import Fernet as frt

KEY = "hBU9lesroX_veFoHz-xUcaz4_ymH-D8p28IP_4rtjq0="
CIPHER = """ gAAAAABaDDCRPXCPdGDcBKFqEFz9zvnaiLUbWHqxXqScTTYWfZJcz-WhH7rf_
             fYHo67zGzJAdkrwATuMptY-nJmU-eYG3HKLO9WDLmO27sex1-R85CZEFCU="""

F = frt(KEY)
OUTPUT = F.decrypt(CIPHER.encode('utf-8'))
OUTPUT_DECODED = OUTPUT.decode('utf-8')
print('Decoded: {0}'.format(OUTPUT_DECODED))
