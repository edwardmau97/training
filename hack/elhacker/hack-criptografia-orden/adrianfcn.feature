## Version 2.0
## language: en

Feature: orden - criptografia - elhacker
  Site:
    http://warzone.elhacker.net/pruebas.php?p=6
  User:
    adrianfcn (Wechall)
  Goal:
    Decrypt the message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have access to "http://warzone.elhacker.net/cifrados/kass.php"

  Scenario: Success: Deciphering a message
    Given I have a message encrypted
    """
    A1E4E8U11E12E15A16E19O22A25O28A31A34E38E40I44A47O49
    N2T3S5D7Q10L14S17S20T21Y24L27H30B32R33S35D37S39N41C42R43P45T46D48 /6/9/13/
    18/23/26/29/36
    """
    And I see that each character in the message is follow for a number
    When you ordered the message according to the number that precedes it
    Then I can see that message becomes this
    """
    A1N2T3E4S5/6D7E8/9Q/10U/11E12/13L14E15A16S17/18E19S20T21O22/23Y24A25
    /26L27O28/29H30A31B32R33A34S35/36D37E38S39E40N41C42R43I44P45T46A47D48O49
    """
    When I remove the numbers and change "/" for spaces
    Then the message was decrypted
    """
    ANTES DE QUE LEAS ESTO YA LO HABRAS DESENCRIPTADO
    """
