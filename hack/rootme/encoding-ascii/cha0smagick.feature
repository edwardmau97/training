## Version 2.0
## language: en

Feature: Encoding-ASCII
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    Cha0smagick
  Goal:
    Decode the string

  Background:
  Hacker's software:
    | <Name>        | <Version>  |
    | Windows 10    | Home       |
    | Google Chrome | 76.0.3     |
  Machine information:
    Given I am registered in root-me
    Given the url "https://www.root-me.org"

  Scenario: Successful: used online converter
    Given I obtain an ASCII codified string from the URL
    When I find an ASCII translator from "https://www.dcode.fr/ascii-"
    Then I try to decrypt the ASCII string by pasting it on the web
    When I get another string as a result
    Then I can actually read the flag from the answer string
    And I conclude that "https://www.dcode.fr/ascii-code" ASCII worked
    And I solved the challenge
    And I can read the flag
