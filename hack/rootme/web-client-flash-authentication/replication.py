"""
    Module responsible of hexadecimal/
    reversing utilities.
    >pylint replication.py
    ************* Module replication
    replication.py:5:0: C0103: Function name "toHex" doesn't conform
     to snake_case naming style (invalid-name)
    ------------------------------------------------------------------
    Your code has been rated at 8.75/10 (previous run: 7.50/10, +1.25)
"""


def reverse(string):
    """
        Return string, reversed
    """
    out = ""
    length = len(string)-1
    for i in range(length+1):
        out += string[length-i]
    return out
