## Version 2.0
## language: en

Feature: Bye intruder - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/bye-intruder.py
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see a text with a hint about the challenge

Scenario: Fail: Wrong procedure
    Given The hint of the challenge
    When I try the username that is shows at the hint
    Then I see that my solution is wrong

  Scenario: Success: Solving challenge
    Given The hint of the challenge
    When I read all the hint
    Then I try to make a Get request to the page
    And I go to an online service for the request in order to do it anonymously
    """
      https://reqbin.com/
    """
    Then I use the service with the parameters GET and the url oh the challenge
    """
    [evidence](request.png)
    """
    Then I get a respone with the challenge
    """
    [evidence](response.png)
    """
    Given The new instructions
    When I look at the table
    Then I see a pattern starting from number 5
    And I realize it continues diagonally as the instructions say
    And I can read the secuence "thesolutionis<FLAG>"
    Then I extract the flag from the sequence which is "<FLAG>"
    Then I solve the challenge
