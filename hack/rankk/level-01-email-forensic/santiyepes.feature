# language: en

Feature: Email forensic
  From site rankk.org
  From Level 1 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/email-forensic.py
  Message: You need to click on send to receive the challenge message.
  Objetive: Find the answer of the challenge
  Evidence: An email sent from the challenge
  """

Scenario: Looking for the challenge
The page shows that I must click to send the challenge to the mail
  Then I click on the hyperlink and I receive the answer:
  """
  The challenge message has been sent to *******07@gmail.com
  """
  Then I initiate session in the mail to which sent the challenge
  And I successfully receive the mail with the instructions of the challenge

Scenario: Solving the challenge
The email tells me to look for the Message-id as a possible solution
  Then I deduce that I should look in the headers of the received mail
  And I'm looking for the best option for this would be:
  """
  Show the message in original body.

  Gmail case: For this we are located in the mail body and on the right
  side we look for an arrow that shows us more options and we select
  'Show original'

  Hotmail case: For this we are located in the mail body and on the right
  side we look  for an arrow that shows us more options and select
  'See origin of the message'
  """
  Then we look for the Header with the Message-id
  And we are successful in finding it
  """
  From: noreply@rankk.org
  Subject: Challenge 1/51
  Message-Id: <20180609050056.02D476157ECA6@smtp.webfaction.com>
  Date: Sat
  """
  Then we copy and paste all the Message-id in the text field of the challenge
  """
  <20180609050056.02D476157ECA6@smtp.webfaction.com>
  """
  And I solve the challenge
