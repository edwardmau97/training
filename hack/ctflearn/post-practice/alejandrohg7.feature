## Version 2.0
## language: en

Feature: post-practice -web -ctflearn
  Site:
    ctflearn.com
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
    | Curl            | 7.58.0      |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: cookie manipulation
    Given I am in the challenge page
    When I inspect the elements of the page
    Then I can not see the cookies stored in my browser
    Then I can not get the flag

  Scenario: Success: POST request with Curl
    When I see the source code of the challenge page
    Then I can see the code
    """
    <body><h1>This site takes POST data that you have not submitted!</h1>
    <!-- username: admin | password: 71urlkufpsdnlkadsf --></body>
    """
    And I can see a username and a password field with the respective values
    And I use Curl to make the POST request with the values
    """
    curl -d "username=admin&password=71urlkufpsdnlkadsf" -X POST
    http://165.227.106.113/post.php
    """
    And I get the flag
