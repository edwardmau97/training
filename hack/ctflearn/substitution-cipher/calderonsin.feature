## Version 2.0
## language: en

Feature: subtitution-cipher - crypto - ctflearn
  Code:
    238
  Site:
    ctflearn
  Category:
    Crypto
  User:
    Calderonsin
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 16.01        |
    | Firefox         | 71.0         |
  Machine information:
    Given I am accessing the challenge site via browser

  Scenario: Succes:Decode the ciphertext using a frequency table
    When I check the challenge's title
    Then I realize it was a ciphertext
    When I search for a method to decrypt the ciphertext
    Then I find the frequency analysis method
    """
    https://crypto.interactive-maths.com/frequency-analysis-breaking-the-
    code.html
    """
    When I start using this methods
    Then I use gedit to found the 4 most used letters in the ciphertext
    And  I realize it was T,A,M and L in that order.
    Then I replace e for T because is the most used letter in english
    Then I asumme the first word was the
    Then I replace t for M and h for I
    Then I look into the frequency table [evidence](frequency.png)
    And  I find that a is the third most used word in english
    Then I replace a for A
    Then I asumme the second word is flag
    Then I replace f for Y, l for S, and g for U
    Then I search for word with less than 4 letters with a
    And  I find haL
    And  I replace s for L
    When I look into the first third words
    """
    the flag Os
    """
    Then  I realize the three word is is
    Then I replace i for O
    When I search for G
    Then I realize that is the fifht word most used
    Then I look into the table frecuency
    And  I found that O is the 4 most used word in english
    Then I replace o for G
    When I search for B
    Then I find the word stBle
    And  I realize B is y
    And  I replace y for B
    When I search for words with e
    Then I found geFeKateR
    And  I assumme is generated
    Then I replace n for F, r for K, d for D
    When I look to first 4 words
    """
    the flag is ifonlyDodernEryHtoCasliQethis
    """
    Then I realize D is m and Q is k
    When I search for words with E
    Then I find Ehain and EontinWed
    Then I realize E is c and W is u
    When I look into the word ifonlymoderncryHtoCaslikethis
    Then I realize that H is p and C is w
    Then I find the flag
    And I submit it and solve the challenge
