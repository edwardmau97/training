## Version 2.0
## language: en

Feature: 27-Basic-enigmagroup
  Site:
    enigmagroup
  User:
    jpverde
  Goal:
    Get the correct login

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0        |
  Machine information:
    Given A login form located in a web page
    """
    http://challenges.enigmagroup.org/basics/um/1/
    """
    And A web page that only allows access if you type the correct password
    And The webpage says
    """
    Administrator Login
    """

  Scenario: Success:manipulate-url
    Given I am in the webpage
    When I press the submit button
    Then I get redirected to another webpage with this message
    """
    Error finding in password.php
    """
    And I can see the url changed to
    """
    http://challenges.enigmagroup.org/basics/um/1/index.php?file=login.php
    """
    When I change the value in the file from "login.php" to "password.php"
    Then I can see in the webpage this message
    """
    administrator:3e0f84
    """
    When I try this password in the login form
    Then I succesfully passed the login form
    And I completed the challenge
