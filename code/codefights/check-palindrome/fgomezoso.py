# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program checks if a given string is a palindrome
"""
from __future__ import print_function


def check_palindrome(input_string):

    """
    Builds the string in the opposite direction
    and it checks if both strings are equal
    """

    reverse_string = ""

    length = len(input_string)

    for i in range(0, length):

        reverse_string = input_string[i] + reverse_string

    return bool(input_string == reverse_string)


def main():

    """
    Returns true when the string is palindrome
    """

    data = open("DATA.lst", "r")

    string = data.readline().rstrip('\n')

    if check_palindrome(string):
        print("true")
    else:
        print("false")

    data.close()


main()

# $ python fgomezoso.py
# true
