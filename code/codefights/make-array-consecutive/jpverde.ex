# $ mix credo
# Checking 2 source files ...
# Analysis took 0.2 seconds (0.07s to load, 0.1s running checks)
# 4 mods/funs, found no issues.

defmodule Statues do
  @moduledoc """
    Solution to codefights problem adjacentElementProduct
  """
  def data do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    _dat = nums |> Enum.map(&String.to_integer/1)
  end

  def result do
  statues = Sums.data
  max_stat = statues |> Enum.max
  min_stat = statues |> Enum.min
  len = length(statues)
  (max_stat - min_stat) - len + 1
  end
end

# $ iex
# iex(1)> c("jpverde.ex")
# iex(2)> Statues.result
# 3
