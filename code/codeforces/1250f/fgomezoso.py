# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the minimum perimeter in meters of a data center
with n square meters. The lengths of all its sides must be integers.
"""
from __future__ import print_function


def is_prime(num1):

    """
    Returns true if the input number is prime. For this challenge
    1 will be considered as prime.
    """

    count = 0

    for i in range(1, num1+1):

        if num1 % i == 0:

            count += 1

    return bool(count <= 2)


def divisors_list(num2):

    """
    Returns the list of divisors for a given number. 1 and the number
    itself aren't considered in the list.
    """

    result = []

    for i in range(2, num2):

        if num2 % i == 0:

            result.append(i)

    return result


def main():

    """
    Checks if the number is prime or composite and returns a different
    calculation for every case.

    If the composite number doesn't have a square root, then its necessary
    to calculate a list of divisors and use brute force to search for 2
    numbers in order to calculate the minimum perimeter.
    """

    data = open("DATA.lst", "r")
    num = int(data.readline().rstrip('\n'))

    perimeter = 0

    if is_prime(num):

        perimeter = (1+num)*2

    else:

        for i in range(2, num):

            if i*i == num:

                perimeter = i*4
                break

    if perimeter == 0:

        divisors = divisors_list(num)

        perim_array = []

        for item1 in divisors:

            for item2 in divisors:

                if item1*item2 == num:

                    perimeter2 = (item1 + item2)*2

                    perim_array.append(perimeter2)

        if len(perim_array) == 1:

            perimeter = perim_array[0]

        else:

            min_perimeter = perim_array[0]

            for item in perim_array:

                if item < min_perimeter:

                    min_perimeter = item

            perimeter = min_perimeter

    print(perimeter)

    data.close()


main()

# $ python fgomezoso.py
# 1154
