object JuanDavidRobles282A {
  def main(args: Array[String]): Unit = {
    import scala.io.StdIn

    var lines : Int = StdIn.readInt()

    var command : String = ""
    var count : Int = 0

    for (i <- 0 until lines){
      command = StdIn.readLine()
      if (command contains("+")){
        count = count + 1
      } else {
        count = count - 1
      }
    }

    println(count)
  }
}
