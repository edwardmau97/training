"""
$ pylint calderonsin.py
Global evaluation
-----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def bob():
    """"max possible function. find the max value with certain input"""
    data = open('DATA.lst', 'r')
    ntime = int(data.readline())
    profits = [0 for i in range(2009)]
    value = 0
    for i in range(ntime):
        info = data.readline()
        info = info.split()
        order = info[0]
        number = int(info[1])
        if order == "win":
            profits[number] = value + 2**number
        else:
            value = max(profits[number], value)
        i = i + 1
    print value


bob()


# $ python3 calderonsin.py build
# 1056
