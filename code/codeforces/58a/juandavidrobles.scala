object JuanDavidRobles58A {
  def main(args: Array[String]): Unit = {

    import scala.io.StdIn
    import scala.util.control.Breaks._

    var string: String = StdIn.readLine()
    val comparingString = "hello"

    var count: Int = 0
    var beforeChar: Char = '0'
    var flag: Boolean = false
    var out: String = "NO"
    var index: Int = 0

    breakable{
      for (i <- 0 until 5){

        index = string.indexOf(comparingString.charAt(count))
        if (index != -1 && !flag){
          count += 1
          if (count == 4){
            string = string.substring(index)
          } else {
            string = string.substring(index+1)
          }
        } else {
          break()
        }
      }
    }


    if (count == 5){
      println("YES")
    } else {
      println("NO")
    }


    /*for (i <- 0 until string.length){

      if (string.charAt(i) == comparingString.charAt(count) && !flag){

          count += 1
          if (count == 4){
            out = "YES"
            flag = true
          }
      }

      if (beforeChar != '0' && (beforeChar != comparingString.charAt(count) ||
        beforeChar != comparingString.charAt(count-1))){
        flag = true
      }
    }*/

    //println(out)

  }
}
