"""
$ pylint calderonsin.py
Global evaluation
-----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
from fractions import Fraction


def main():
    """Main function"""
    data = open('DATA.lst', 'r')
    data = data.readline()
    data = data.split()
    yakko = int(data[0])
    wakko = int(data[1])

    if yakko >= wakko:

        number = yakko
        if number == 7:
            print "0/1"
        elif number == 1:
            print "1/1"

        else:
            number = 6-yakko+1
            result = Fraction(number, 6)
            print result
    else:
        number = wakko

        if number == 7:
            print "0/1"
        elif number == 1:
            print "1/1"
        else:
            number = 6-wakko+1
            result = Fraction(number, 6)
            print result


main()


# $ python3 calderonsin.py build
# 0/1
