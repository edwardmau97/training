/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');


function jumpingOnClouds(arr) {
  let result = 0;
  for (let i = 0; i < arr.length; i++) {
    if ((i + 2) < arr.length && arr[i + 2] === 0) {
      i++;
    }
    result++;
  }
  return result - 1;
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const clouds = inputData
    .split('\r\n')
    .slice(1)
    .map((x) => x.split(' '))
    .flat()
    .map((x) => Number(x));

  console.log(jumpingOnClouds(clouds));
});

/*
$ js ngarci
output:
6
*/
