
/*
'eslint --init' #linting config
'eslint henrymej.js' #linting
*/

/* global BigInt */
/* eslint brace-style: ["error", "stroustrup"] */

const files = require('fs');
const path = require('path');

function helper(first, second, third) {
  let auto = BigInt(first);
  let reducer = second;
  let result = BigInt(1);
  while (reducer > 0) {
    if (reducer % 2 === 0) {
      auto = (auto * auto) % BigInt(third);
      reducer = Math.floor(reducer / 2);
    }
    else {
      result = (auto * result) % BigInt(third);
      reducer -= 1;
    }
  }
  return Number(result);
}

function solve(param) {
  const defaultreturn = 4;
  const modulus = 1000000007;
  if (param === 0) {
    return defaultreturn;
  }
  return (2 + helper(2, param + 1, modulus));
}

files.readFile(path.join(__dirname, 'DATA.lst'), (flaw, stream) => {
  if (flaw) {
    throw flaw;
  }
  const inputs = stream.toString().split('\n');
  const [ total ] = inputs;
  let outputs = '';
  for (let index = 1; index <= total; index++) {
    const base = 10;
    const outp = solve(parseInt(inputs[index], base));
    outputs = `${ outputs } ${ outp }`;
  }
  outputs = outputs.substring(1, outputs.length);
  files.writeFile('Output.txt', outputs, (issue) => {
    if (issue) {
      throw issue;
    }
  });
});

/*
node henrymej.js
595845305 191690601 383381200 766762398 533524787 67049565 134099128
268198254 536396506 72793003 145586004 291172006 582344010 164688011
329376020 658752038 317504067 635008132 270016255 540032508 80065007
160130012 320260022 640520042 281040075 562080148 124160287 248320572
496641142 993282282 986564555 973129101 946258193 892516377 785032745
570065481 140130953 280261904 560523806 121047603 242095204 484190406
968380810 936761611 873523213 747046417 494092825 988185648 976371287
952742565
*/
