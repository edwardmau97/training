
/*
tslint --init # init linter config
tslint henrymej.ts #lint
tsc henrymej.ts  #compile to js
*/

const fs = require("fs");

function gcd(a, b) {
  if (a === b) {
    return a;
  }
  else if (a > b) {
    return gcd(a - b, b);
  }
  else {
    return gcd(a, b - a);
  }
}

let squares = [];
for (let i = 1; i <= 500; i++) {
  squares[i - 1] = Math.pow(2 * i, 2);
}

function solve(n) {
  let cnt = 0;
  let cnt2 = 0;
  const limit = Math.floor(Math.sqrt(n)) + 1;
  for (let i = 1; i < limit; i++) {
    if (n % i === 0) {
      if (n / i === i) {
        cnt += 1;
      }
      else {
        cnt += 2;
      }
    }
  }
  let div;
  for (div of squares) {
    if (n % div === 0 && div < n) {
      cnt2 += 1;
    }
  }
  if (cnt2 === 0) {
    const zero = 0;
    return zero.toString();
  }
  cnt -= 1;
  const c = gcd(cnt2, cnt);
  cnt2 /= c;
  cnt /= c;
  return cnt2.toString() + "/" + cnt.toString();
}

fs.readFile(__dirname + "/DATA.lst", (err, data) => {
  if (err) {
    throw err;
  }
  const inputs = data.toString().split("\n");
  const T = inputs[0];
  let outputs = "";
  for (let index = 1; index <= T; index++) {
    outputs = outputs + " " + solve(inputs[index]);
  }
  outputs = outputs.substring(1, outputs.length);
  console.log(outputs);
});

/*
node henrymej.js
0 0 0 0 0 0 0 1/47 0 8/107 2/79 2/19 0 0 0 1/5 0 1/23 0 0 0 1/31 0 2/47
1/9 0 0 0 0 1/7 0 0 0 4/39 0 0 0 2/17 1/5 2/35 0 0 0 0 2/143 1/23 0 0 0 0
*/
