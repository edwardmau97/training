/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');


function migratoryBirds(arr) {
  const countTypes = [ 0, 0, 0, 0, 0, 0 ];
  arr.forEach((x) => {
    countTypes[x] += 1;
  });
  return countTypes.indexOf(Math.max(...countTypes));
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const birds = inputData
    .split('\r\n')
    .slice(1)
    .map((x) => x.split(' '))
    .flat()
    .map((x) => Number(x));

  console.log(migratoryBirds(birds));
});

/*
$ js ngarci
output:
3
*/
