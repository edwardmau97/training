/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

function isValid(funnyString) {
  const obj = {};
  const arr = funnyString.split('');
  arr.forEach((x) => {
    if (obj[x] >= 1) {
      obj[x]++;
    }
    else {
      obj[x] = 1;
    }
  });
  const vals = Object
    .values(obj)
    .sort((eleOne, eleTwo) => eleTwo - eleOne);
  const arrUniquesVals = [ ...new Set(vals) ];
  const reduceArr = arrUniquesVals.reduce((x, y) => x - y);
  const subArrs = arrUniquesVals.map((x) => vals.filter((ele) => ele === x));
  if (arrUniquesVals.length === 1) {
    return 'YES';
  }
  if (arrUniquesVals.length > 2) {
    return 'NO';
  }
  if (subArrs[0].length === 1 || subArrs[1].length === 1) {
    if (reduceArr === 1 ||
       (subArrs[0][0] === 1 && subArrs[0].length === 1) ||
       (subArrs[1][0] === 1 && subArrs[1].length === 1)) {
      return 'YES';
    }
    return 'NO';
  }
  return 'NO';
}

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ funnyString ] = inputData.split('\r\n');

  console.log(isValid(funnyString));
});

/*
$ js ngarci
output:
YES
*/
