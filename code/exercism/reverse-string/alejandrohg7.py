#!/usr/bin/env python3
'''
$pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This script open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        string_to_revers = []
        for line in opened_file:
            string_to_revers.append(line.strip())
        return string_to_revers


def reverse(strings_to_reverse):
    '''This script reverse the string and print it'''
    for string_data in strings_to_reverse:
        len_text = len(string_data)
        each_letter = [letter for letter in string_data]
        index_letter = [index_number for index_number in range(len_text)]
        index_letter.sort(reverse=True)
        r_string = [each_letter[number] for number in index_letter]
        r_string = "".join(str(to_join_letter) for to_join_letter in r_string)
        print(r_string)


reverse(open_file())
# $ python3 alejandrohg7.py
# tobor
# nemaR
# !yrgnuh m'I
# racecar
# reward
