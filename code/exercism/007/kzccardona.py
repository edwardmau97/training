"""
This code determines if a word is an isogram
>pylint kzccardona.py

    -------------------------------------------------------------------
    Your code has been rated at 10.00/10

"""
from __future__ import print_function


def is_isogram(word):

    """
    This function determines if a word is an isogram
    """
    letters = list(word.lower())
    val = 0
    if word:
        for position in enumerate(letters):
            for every in range(position[0]+1, len(letters)):
                if letters[position[0]] == letters[every] \
                        and letters[position[0]] != " " \
                        and letters[position[0]] != "-":
                    val = 1
                    break
                else:
                    val = 2

            if val == 1:
                break
    else:
        val = 1

    if val == 1:
        return False

    return True


for words in open("DATA.lst", "r"):
    characters = words.rstrip('\n')
    result = (is_isogram(characters))
    print(result)


# > python ./kzccardona.py
# True
# False
# True
