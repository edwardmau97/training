#!/usr/bin/env python3
'''
$ pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file '''
    with open('DATA.lst', 'r') as opened_file:
        list_of_lists = []
        cases = []
        for row in opened_file:
            list_of_lists.append(row.split())
        list_of_lists = list_of_lists[1:]
        for no_tosses, tosses in zip(list_of_lists[0::2], list_of_lists[1::2]):
            cases.append((no_tosses, tosses))
        return cases


def penney_game(cases_tosses):
    '''This function count occurrences of each three-coin sequence'''
    no_case = cases_tosses[0][0]
    eval_tosses = cases_tosses[1][0]
    guide_tosses = cases_tosses[1][0]
    mix_tosses = []
    try:
        index_base = -1
        while eval_tosses:
            mix_tosses.append((guide_tosses[index_base] + eval_tosses[0:2]))
            eval_tosses = eval_tosses[1:]
            index_base += 1
    except IndexError:
        pass
    mix_tosses = mix_tosses[1:-1]
    counter_ttt = mix_tosses.count('TTT')
    counter_tth = mix_tosses.count('TTH')
    counter_tht = mix_tosses.count('THT')
    counter_thh = mix_tosses.count('THH')
    counter_htt = mix_tosses.count('HTT')
    counter_hth = mix_tosses.count('HTH')
    counter_hht = mix_tosses.count('HHT')
    counter_hhh = mix_tosses.count('HHH')
    print(no_case, counter_ttt, counter_tth, counter_tht, counter_thh,
          counter_htt, counter_hth, counter_hht, counter_hhh)


for each_case in open_file():
    penney_game(each_case)

# $ python3 alejandrohg7.py
# 1 0 0 0 0 0 0 0 38
# 2 38 0 0 0 0 0 0 0
# 3 4 7 6 4 7 4 5 1
# 4 6 3 4 5 3 6 5 6
