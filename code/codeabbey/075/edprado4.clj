(ns yatchordicepoker.core
  (:gen-class))

(defn yachtgame [arr]
  (def freq (frequencies arr))
  (def game true)
  (def dos false)
  ;caso small-straight
  (if (and(not= (freq 1) nil)(not= (freq 2) nil)(not= (freq 3) nil)(not= (freq 4) nil)(not= (freq 5) nil)(== (freq 1) 1) (== (freq 2) 1) (== (freq 3) 1) (== (freq 4) 1)(== (freq 5) 1))
    (do (print "small-straight ")
        (def game false)))
  ;caso big straight
  (if (and (not= (freq 6) nil)(not= (freq 2) nil)(not= (freq 3) nil)(not= (freq 4) nil)(not= (freq 5) nil)(== (freq 6) 1) (== (freq 2) 1) (== (freq 3) 1) (== (freq 4) 1) (== (freq 5) 1))
    (do (print "big-straight ")
        (def game false)))
  (dotimes [i 6]
    (def n (inc i))
    (if (not= (freq n) nil)
      (do
        ;caso yatch
        (if (and (== (freq n) 5) (= game true))
          (do (print "yacht ")
            (def game false)))
        
        ;caso two-pairs / full-house
        (if (== (freq n) 2) 
          (do (def dos true) 
            (dotimes [j 6]
              (def m (inc j))
              (if (and (not= (freq m) nil)(> (freq m) 1) (not= m n) (= game true))
                (do (if (== (freq m) 2)
                      (do (print "two-pairs ")
                        (def game false))
                      (do (print "full-house ")
                        (def game false))))))))  
        
        ;caso three/fullhouse
        (if (and (== (freq n) 3) (= game true))
          (do 
            (dotimes [j 6]
              ;(println "j: " j)
              (if (not= (inc j) n) 
                (if (and (not= (freq (inc j)) nil) (= game true)) 
                  (do 
                    (if (and (== (freq (inc j)) 2) (= game true))
                      (do (print "full-house ")
                        (def game false))
                      (do 
                        (print "three ")
                        (def game false)))))))))
        
        ;caso four
        (if (and (== (freq n) 4) (= game true)) (do (print "four ") (def game false)))))
    
    ;caso pair
    (if (and (= dos true) (== n 6) (= game true)) (do (print "pair ") (def game false)))
    ;caso none
    (if (and (== n 6)(= game true) (= dos false)) (print "none "))))

(defn -main []
  (def data [[2 3 4 5 1] [2 2 4 3 2] [6 3 5 1 4] [4 2 2 1 4] [5 5 5 5 5] [4 6 4 6 1] [2 3 4 5 6] [2 3 4 5 6] [4 3 5 4 5] [2 3 4 5 6] [3 3 3 3 3] [2 3 4 5 6] [2 3 4 5 1] [6 6 6 6 6] [4 4 4 2 3] [5 4 5 5 1] [3 4 6 1 1] [3 6 5 5 2] [6 3 5 6 3] [2 3 4 5 1] [4 6 1 5 3] [3 2 3 3 6] [1 5 5 1 4] [2 3 4 5 1] [2 3 4 5 6] [2 3 4 5 1]])
  (dotimes [i (count data)]
    (yachtgame (data i))))

