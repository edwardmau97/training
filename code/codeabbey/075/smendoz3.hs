-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (checkAll inputs))

checkAll :: [String] -> [String]
checkAll = map function

function :: String -> String
function x = combination (diceArr x)

checkString :: String -> [Int]
checkString str = map read $ words str :: [Int]

combination :: [Int] -> String
combination arr
  | 5 `elem` arr                  = "yacht"
  | 4 `elem` arr                  = "four"
  | 3 arr && elem 2 arr = "full-house"
  | 3 `elem` arr                  = "three"
  | arr == [1,1,1,1,1,0]        = "small-straight"
  | arr == [0,1,1,1,1,1]        = "big-straight"
  | 2 `elem` arr                  = doubles arr 0
  | otherwise                   = "none"

doubles :: [Int]-> Int -> String
doubles [x] pair =
  if (pair > 1) || (x == 2 && pair == 1) then
    "two-pairs"
  else
    "pair"
doubles (x:xs) pair =
  if x == 2 then
    doubles xs (pair + 1)
  else
    doubles xs pair

diceArr :: String -> [Int]
diceArr x = sortDice (checkString x) 0 0 0 0 0 0

sortDice :: [Int] -> Int -> Int -> Int -> Int -> Int -> Int -> [Int]
sortDice [x] one two three four five six
  | x == 1    = [one+1,two,three,four,five,six]
  | x == 2    = [one,two+1,three,four,five,six]
  | x == 3    = [one,two,three+1,four,five,six]
  | x == 4    = [one,two,three,four+1,five,six]
  | x == 5    = [one,two,three,four,five+1,six]
  | otherwise = [one,two,three,four,five,six+1]
sortDice (x:xs) one two three four five six
  | x == 1    = sortDice xs (one+1) two three four five six
  | x == 2    = sortDice xs one (two+1) three four five six
  | x == 3    = sortDice xs one two (three+1) four five six
  | x == 4    = sortDice xs one two three (four+1) five six
  | x == 5    = sortDice xs one two three four (five+1) six
  | otherwise = sortDice xs one two three four five (six+1)

-- $ ./smendoz3
--   four three small-straight pair big-straight two-pairs small-straight
--   pair pair pair yacht yacht big-straight four two-pairs big-straight
--   pair three small-straight small-straight two-pairs pair big-straight
--   pair small-straight pair small-straight
