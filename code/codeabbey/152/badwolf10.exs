'''
$mix credo --strict badwolf10.exs #linting
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.2 seconds (0.01s to load, 0.2s running checks)
9 mods/funs, found no issues.

$mix compile badwolf10.exs #compilation
'''

defmodule Main do
  @moduledoc """
  Main module
  """
  def modinv(num, mod) do
    egcd(num, mod, 0, 1 , mod)
  end

  def egcd(rn, rc, tc, tn, mod) do
    if rn == 0 do
      if tc < 0 do
        tc + mod
      else
        tc
      end
    else
      q = div rc, rn
      ta = tn
      ra = rn
      tn_n = tc - q * tn
      tc_n = ta
      rn_n = rc - q * rn
      rc_n = ra
      egcd(rn_n, rc_n, tc_n, tn_n, mod)
    end
  end

  def emod(a, b, c) do
    cond do
      b == 0 ->
        1
      rem(b,2) == 0 ->
        d = emod(a, div(b,2), c)
        rem(d * d, c)
      true ->
        rem(rem(a, c) * emod(a, b - 1, c), c)
    end
  end
  def decypher(cypher, d, n) do
    emod(cypher, d, n)
  end
  def getdecypheredtext(cypher, e, phi, n) do
    decodeascii(splitbytwo(cypher, e, phi, n))
  end
  def splitbytwo(cypher, e, phi, n) do
    data = Integer.to_string(decypher(cypher, modinv(e, phi), n))
    data |> String.codepoints
    |> Enum.chunk(2)
    |> Enum.map(&Enum.join/1)
  end
  def decodeascii(charlist) do
    if Enum.empty? charlist do
    else
      IO.puts String.trim <<String.to_integer(List.first(charlist)) :: utf8>>
      decodeascii(List.delete_at(charlist, 0))
    end

  end
  def getparams(plist, index) do
    if index == 0 do
      hd plist
    else
      getparams(List.delete_at(plist, 0), index - 1)
    end
  end
  end

{:ok, contents} = File.read("DATA.lst")
stringlist = String.split(contents, "\n", trim: true)
plist = stringlist |> Enum.map(&String.to_integer/1)
e = 65_537
p = Main.getparams plist, 0
q = Main.getparams plist, 1
cypher = Main.getparams plist, 2
n = p * q
phi = n - p - q + 1

Main.getdecypheredtext(cypher, e, phi, n)

'''
$elixirc badwolf10.exc
T
O
W
E
L

G
L
O
V
E

P
U
P
P
Y

C
H
E
C
K

F
A
I
T
H

B
E
A
N
S
'''
