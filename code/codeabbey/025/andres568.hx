/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var inputData:Array<String> = [];
    var result:String = "";
    var xnext:Int = 0;
    var xcur:Int = 0;
    var a:Int = 0;
    var c:Int = 0;
    var m:Int = 0;
    var n:Int = 0;
    lines.pop();
    lines.shift();

    for (i in 0...lines.length) {
      inputData = lines[i].split(" ");
      xcur = Std.parseInt(inputData[3]);
      a = Std.parseInt(inputData[0]);
      c = Std.parseInt(inputData[1]);
      m = Std.parseInt(inputData[2]);
      n = Std.parseInt(inputData[4]);

      for (i in 0...n) {
        xnext = (a * xcur + c) % m;
        xcur = xnext;
      }

      result = result + " " + xnext;
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:46:  286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549
  13339 6
**/
