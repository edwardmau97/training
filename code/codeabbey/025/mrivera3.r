# $ lintr::lint('mrivera3.r')


lcgen <- function() {
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  res <- c()
  for (i in seq_len(nrow(data))) {
    a <- data$X1[i]
    c <- data$X2[i]
    m <- data$X3[i]
    x0 <- data$X4[i]
    n <- data$X5[i]
    for (j in seq_len(n)) {
      xnext <- (a * x0 + c) %% m
      x0 <- xnext
    }
    res <- c(res, xnext)
  }
  return(res)
}
lcgen()

# $ Rscript mrivera3.r
# 286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549 13339 6
