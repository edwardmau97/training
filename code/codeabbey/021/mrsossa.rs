/*
$ rustup run nightly cargo clippy
Compiling mrsossa.rs
$ rustc mrsossa.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::convert::TryInto;

fn main(){
  let file = File::open("DATA.lst").unwrap();
  let reader = BufReader::new(file);
  let mut vec:Vec<i32>  = Vec::new();
  let mut case = 0;
  for lines in reader.lines() {
    let line = lines.unwrap();
    let token:Vec<&str> = line.split(" ").collect();
    if case == 0 {
      case = token[1].parse::<i32>().unwrap();
      vec = Vec::with_capacity(case.try_into().unwrap());
      let mut i = 0;
      while i < case {
        vec.push(0);
        i = i + 1;
      }
    }
    else {
      for x in 0..token.len()-1 {
        for i in (0..case).enumerate() {
          if token[x].parse::<i32>().unwrap() == i.0.try_into().unwrap() {
            vec[i.0] = vec[i.0-1] + 1;
          }
        }
      }
      for i in vec.iter() {
        println!("{}",i);
      }
    }
  }
}

/*
 $ rustc mrsossa.rs
 $ ./mrsossa
 21 24 32 24 23 25 29 27 25 28 16 19 30
*/

