# $ lintr::lint('mrivera3.r')


revstring <- function() {
  data <- read_csv("DATA.lst", col_names = FALSE)
  sep <- strsplit(data$X1[1], "")
  sep <- sep[[1]]
  res <- c(seq_len(length(sep)))
  a <- length(sep) + 1
  for (i in seq_len(length(sep))) {
    res[- (i - a)] <- sep[i]
  }
  res <- noquote(paste(res, collapse = ""))
  return(res)
}
revstring()

# $ Rscript mrivera3.r
# sutcac tuoba ydrapoej flehs ffo reppus eraf no
