/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn smoothing_weather(temperature: Vec<f64>) {
  let len = temperature.len();
  print!("{} ", temperature[0]);
  for i in 1..len - 1 {
    let p = (temperature[i - 1] + temperature[i] + temperature[i + 1]) / 3
    as f64;
    print!("{} ", p);
  }
  println!("{}", temperature[len - 1]);
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  let mut temperature = Vec::<f64>::new();
  lines.next();
  for line in lines {
    for c in line.split_whitespace() {
      temperature.push(c.trim().parse::<f64>().unwrap());
    }
  }
  smoothing_weather(temperature);
  Ok(())
}

/*
  ./adrianfcn
  32.6 33 34.6 39.166666666666664 41.46666666666667 43.699999999999996 44.1
*/
