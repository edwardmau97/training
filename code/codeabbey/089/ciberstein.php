<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists("./DATA.lst")) {

  $data = array('C','C#','D','D#','E','F','F#','G','G#','A','A#','B');
  $input = fopen("./DATA.lst", "r");
  $ncases = fgets($input, 65536);
  $obtain = explode(' ', fgets($input, 65536));

  for($i = 0 ; $i < 84 ; $i++) {

    $med = 440 * pow(1.059463094, $i % 12 - 9) * pow(2, (int)($i / 12) - 3);
    $range[$i] = ($last + $med) / 2;
    $last = $med;
  }

  for($r = 0 ; $r < $ncases ; $r++) {

    for($i = 0 ; $i < 84 ; $i++) {

      if($obtain[$r] < $range[$i+1]) {

        print $data[$i%12].((int)($i / 12) + 1).' ';
        break;
      }
    }
  }
}
/*
./ciberstein.php
F#3 A#2 G#3 A1 G#1 F2 A#5 A#3 B1 C2 F#4 G5
*/
?>
