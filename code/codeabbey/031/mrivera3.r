# $ lintr::lint('mrivera3.r')


rotatestring <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  res <- c()
  for (i in seq_len(nrow(data))) {
    splt <- strsplit(data$X2[i], "")
    splt <- splt[[1]]
    amount <- data$X1[i]
    if (amount > 0) {
      a <- head(splt, n = amount)
      b <- tail(splt, n = (length(splt) - amount))
      str <- paste(c(b, a), collapse = "")
    }
    else {
      c <- tail(splt, n = -amount)
      d <- head(splt, n = (length(splt) + amount))
      str <- paste(c(c, d), collapse = "")
    }
    res <- c(res, str)
    res <- noquote(res)
  }
  return(res)
}
rotatestring()

# $ Rscript mrivera3.r
#  whomthebelltollsfor numberverycomplex
