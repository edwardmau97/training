# $ rubocop jdanilo7.rb
# Inspecting 1 file

# 1 file inspected, no offenses detected

def read_file(filename)
  lines = []
  File.foreach(filename) do |line|
    line = line.split(' ')
    nums = []
    line.each { |num| nums.push(num.to_i) }
    lines.push(nums)
  end
  lines
end

def process_input(input)
  sums = ''
  input.each do |nums|
    powers = nums.map { |n| n**2 }
    sum = 0
    powers.each { |p| sum += p }
    sums += sum.to_s + ' '
  end
  sums
end

input = read_file('DATA.lst')
input.shift
puts process_input(input)

# $ ruby jdanilo7.rb
# 1649 860 1527 10 45 29 983 1135 1010 40 84 467 838 920 209 1213 578 261
