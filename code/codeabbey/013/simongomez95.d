/*
➜  013 git:(sgomezatfluid) ✗ dscanner -S sgomezatfluid.d
➜  013 git:(sgomezatfluid) ✗ dscanner -s sgomezatfluid.d
➜  013 git:(sgomezatfluid) ✗ dmd sgomezatfluid.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;
import std.algorithm;
import std.array;

void main() {
  File file = File("DATA.lst", "r");
  file.readln();
  string[] numberstring = file.readln().split(" ");
  int[] numberarr = getNumberArr(numberstring);
  string answer;
  int sum = 0;
  foreach(int num; numberarr) {
    int[] decomposed = decomposeInt(num);
    for(int i=0; i < decomposed.length; i++) {
      sum = sum+(decomposed[i]*(i+1));
    }
    answer ~= to!string(sum) ~ " ";
    sum = 0;
  }
  writeln(answer);
  file.close();
}

private int[] decomposeInt(int value) {
  int[] decomposed = [];
  while (value > 0) {
    const int digit = value % 10;
    decomposed.insertInPlace(0, digit);
    value /= 10;
  }
  return decomposed;
}

private int[] getNumberArr(string[] numberstring) {
  int[] numberarr;
  foreach(string numstr; numberstring) {
    numberarr ~= to!int(stripws(numstr));
  }
  return numberarr;
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
➜  013 git:(sgomezatfluid) ✗ ./sgomezatfluid
38 118 253 154 27 15 139 8 2 38 34 60 21 6 138 113 128 204 179 128 8 224 46 1 40
 172 68 110 31 142 26 15 37 1 170 9
*/
