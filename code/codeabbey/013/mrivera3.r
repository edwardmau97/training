# $ lintr::lint('mrivera3.r')


library(readr)
wsum <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  sep <- as.character(data)
  spl <- strsplit(sep, "")
  res <- c()
  for (i in seq_len(length(spl))) {
    res <- c(res, sum(as.integer(spl[[i]]) * c(seq_len(length(spl[[i]])))))
  }
  return(res)
}
wsum()

# $ Rscript mrivera3.r
#38 118 253 154  27  15 139   8   2  38  34  60  21   6 138 113 128 204
#179 128 8 224  46   1  40 172  68 110  31 142  26  15  37   1 170   9
