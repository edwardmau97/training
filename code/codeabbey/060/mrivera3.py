'''
$ pylint mrivera3.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.57/10, +0.43)
'''
from random import randint


def swharv():
    "finding the most profitable path"
    data = [line.rstrip('\n') for line in open("DATA.lst")]
    del data[0]
    results = []
    for values in data:
        prov = values.split(sep=" ")
        prov = [int(x) for x in prov]
        l_iterator = 0
        partial = []
        while l_iterator < 100000:
            i = 0
            res = prov[0]
            while i < len(prov)-1:
                i += randint(2, 3)
                if i < len(prov):
                    res += (prov[i])
                else:
                    break
            l_iterator += 1
            partial.append(res)
        results.append(max(partial))
    return results


swharv()

# $ python3 mrivera3.py
# 213 195 112 214 170 170 188 225 160 167 186 126
# 115 147 140 155 200 156
