/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var sequences:Array<String> = cont.split("\n");
    var r:EReg = ~/[^< | ^> | ^( | ^) | ^{ | ^} | ^\[ | ^\]]/g;
    var result:String = "";
    var sequence:String = "";
    var oldSequenceLenght:Int = 0;
    sequences.shift();
    sequences.pop();

    for (i in 0...sequences.length) {
      oldSequenceLenght = 0;
      sequence = r.replace(sequences[i], "").replace(" ", "").replace("^", "");

      while (0 < sequences.length) {
        oldSequenceLenght = sequence.length;
        sequence = sequence.replace("{}", "");
        sequence = sequence.replace("[]", "");
        sequence = sequence.replace("<>", "");
        sequence = sequence.replace("()", "");
        if (sequence.length == 0) {
          result = result + "1 ";
          break;
        }
        if (oldSequenceLenght == sequence.length) {
          trace(sequence, sequence.length);
          result = result + "0 ";
          break;
        }
      }
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:48: 0 1 1 0 0 1 0 1 1 0 0 0 0 0 1 0 0 0
**/
