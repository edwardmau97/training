#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* jshint ignore:start */

/* global print */

const [ input ] = arguments;
const testCases = input.split('\n');
const total = testCases.shift();

const brackets = new RegExp('[^(){}\\[\\]<>]', 'g');
const opening = [ '(', '[', '{', '<' ];
const closing = [ ')', ']', '}', '>' ];

const answer = [];

for (let testCase = 0; testCase < total; testCase++) {
  let clean = testCases[testCase].replace(brackets, '');
  for (let char = 0; char < clean.length; char++) {
    const bracket = clean[char];
    const close = closing.indexOf(bracket);
    if (close >= 0 && opening[close] === clean[char - 1]) {
      clean = clean.slice(0, char - 1) + clean.slice(char + 1);
      char = 0;
    }
  }
  answer.push(clean.length > 0 ? 0 : 1);
}

print(answer.join(' '));

/* jshint ignore:end */

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
0 1 1 0 0 1 0 1 1 0 0 0 0 0 1 0 0 0
*/
