(ns medianofthree.core
  (:gen-class))
(defn middlevalue [n]
  (def minimo (apply min n))
  (def maximo (apply max n))
  (dotimes [i (count n)]
    (if (and (> (n i) minimo) (< (n i) maximo)) (def middle (n i))))
  (* middle 1))

(defn -main []
  (def data [[8 174 84] [124 60 130] [740 58 668] [485 66 479] [597 5 14] [101 1 6] [7 47 11] [26 4 955] [979 904 894] [360 357 368] [763 72 761] [4 9 969] [1108 778 1124] [88 333 6] [857 852 825] [585 39 525] [169 117 27] [71 1306 420] [1042 1077 311] [792 19 1012] [51 61 63]])
  (def answer (vec (replicate (count data) -1)))
  (dotimes [j (count data)]
    (def answer (assoc answer j (middlevalue (data j)))))   
  (println "La respuesta es " answer))
