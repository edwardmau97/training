/*
tslint jdiegoc.ts
jdiegoc.ts file  pass linting
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
 const token = line.split(" ");

 if ((+token[0] < +token[1] && +token[0] > +token[2]) ||
  (+token[0] > +token[1] && +token[0] < +token[2])){
     console.log(token[0]);
 }
 if ((+token[1] < +token[0] && +token[1] > +token[2]) ||
  (+token[1] > +token[0] && +token[1] < +token[2])){
     console.log(token[1]);
 }
 if ((+token[2] < +token[0] && +token[2] > +token[1]) ||
  (+token[2] > +token[0] && +token[2] < +token[1])){
    console.log(token[2]);
}
});


/*
 $ts jdiegoc.ts
 4 1087 22 57 503 80 161 811 620 112 952 52 15
 847 13 90 38 36 308 14 971 117 250 284 11 929 151 358
*/
