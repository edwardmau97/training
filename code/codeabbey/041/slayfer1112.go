/**
> golint slayfer1112.go
**/

package main

import (
  "fmt"
  "bufio"
  "strconv"
  "os"
  "strings"
)

func main() {

  dataIn, _ := os.Open("DATA.lst")

  dataScan := bufio.NewScanner(dataIn)

  for dataScan.Scan() {
    line := strings.Split(dataScan.Text()," ")

    if len(line) > 1 {
      b, _ := strconv.Atoi(line[0])
      c, _ := strconv.Atoi(line[1])
      d, _ := strconv.Atoi(line[2])

      if ((c>b) && (b>d)){
        fmt.Printf("%d ", b)
      }
      if ((d>b) && (b>c)){
        fmt.Printf("%d ", b)
      }
      if ((b>c) && (c>d)){
        fmt.Printf("%d ", c)
      }
      if ((d>c) && (c>b)){
        fmt.Printf("%d ", c)
      }
      if ((b>d) && (d>c)){
        fmt.Printf("%d ", d)
      }
      if ((c>d) && (d>b)){
        fmt.Printf("%d ", d)
      }
    }
  }
}

/**
> go run slayfer1112.go
4 1087 22 57 503 80 161 811 620 112 952 52 15 847
13 90 3836 30814 971 117 250 284 11 929 151 358
**/
