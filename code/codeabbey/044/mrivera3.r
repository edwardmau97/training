# $ lintr::lint('mrivera3.r')


doubdice <- function() {
  "simulating a double dice rolling"
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- rowSums(data %% 6 + 1)
  return(results)
}
doubdice()

# $ Rscript mrivera3.r
# 5  7 10  4  9  7  6  4  6  8  3 11 10  9  4  9  5  8  4  8  5  5
