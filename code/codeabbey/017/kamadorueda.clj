; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; checksum of vector
(defn dochecksum
  ([arr c cs]
    (if (= 1 c)
      (let [h  (first arr)
            cs (+ cs h)
            cs (* cs 113)
            cs (mod cs 10000007)]
        cs
      )
      (let [h  (first arr)
            t  (rest arr)
            cs (+ cs h)
            cs (* cs 113)
            cs (mod cs 10000007)]
        (dochecksum t (- c 1) cs)
      )
    )
  )
)

; tokenize whitespace separated string into a vector of integers
(defn stovi
  ([wsstr]
    (let [l (map #(Integer/parseInt %) (clojure.string/split wsstr #" "))
          v (into [] l)]
      v
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [v (stovi line)
              c (count v)]
          (if-not (= 1 c)
            (print (str (dochecksum v c 0)))
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   6617103
