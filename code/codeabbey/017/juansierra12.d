/*
$ dscanner --imports juansierra12.d #linting
std.format
std.stdio
$ dscanner --syntaxCheck juansierra12.d #linting
$ dscanner --styleCheck --skipTests juansierra12.d #linting
$ dmd juansierra12.d #Compilation
*/

import std.stdio;
import std.format;

void main (){
//Initializing variables
  int test_cases;
  double result = 0.0;
  double num = 0.0;

  //Read input from stdin
  readf("%s", &test_cases);

  for (int i=0; i < test_cases; i++) {
    //Read each of the numbers
    readf(" %s", &num);
    //make the checksum
    result += num;
    result *= 113;
    result %= 10000007;
  }
  //Format the output string
  auto output = format!"%.f"(result);
  writeln(output);
}

/*
$ ./juansierra12
1774746
*/
