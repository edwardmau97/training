# gdlint john2104.gd
# Success: no problems found


extends Node


func _ready():
    var input = return_data()
    var rows = input.split("\n")
    var looping = rows[1].split(" ")
    var sum = 0
    for loop in looping:
        sum += int(loop)
    print(sum)

func return_data():
    var file = File.new()
    file.open("res://DATA.lst", File.READ)
    return file.get_as_text()

# Create a scene in godot and attach this script
# Run the scene
#
# ** Debug Process Started **
# OpenGL ES 3.0 Renderer: Mesa DRI Intel(R) HD Graphics 520 (Skylake GT2)
# 126
