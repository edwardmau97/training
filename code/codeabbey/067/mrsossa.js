/*
 $ eslint mrsossa.js
*/
/* global print */

import { readFileSync } from 'fs';

const dat = readFileSync('DATA.lst').toString().split('\n');
for (let ind = 0; ind < dat.length - 1; ind++) {
  if (ind > 0) {
    const numberr = 2.078087;
    const numberrr = 1.672276;
    const fibo = (numberr * Math.log(dat[ind])) + numberrr;
    print(Math.round(fibo));
  }
}

/*
 $ js mrsossa.js
977 442 846 557 800 683 943 360 630 385
702 322 549 693
*/
