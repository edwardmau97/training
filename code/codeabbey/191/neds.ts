/*
$ tslint neds.ts
$ tsc neds.ts
*/

import * as fs from "fs";

const filePath = "DATA.lst";

function reFindMin(mass: string, start: number) {
  let cuMin: number = mass[0].charCodeAt(0);
  let ind: number = -1;
  for (let i = 1; i < mass.length; i++) {
    const cun = mass[i].charCodeAt(0);
    if (cun <= cuMin) {
      cuMin = cun;
      ind = i;
    }
  }
  if (ind !== -1) {
    return ind;
  }
  else {
    return start;
  }
}

function setCharAt(str: string, index: number, chr: any) {
  if (index > str.length - 1) {
    return str;
  }
  return str.substr(0, index) + chr + str.substr(index + 1);
}

function findMaxMin(mass: string) {
  let currentMin: number = 70;
  let currentMax: number = 0;
  let rightOneInd: number = -1;
  let leftOneInd: number = -1;
  let rightTwoInd: number = -1;
  let leftTwoInd: number = -1;
  let currentNumber: number = 0;

  for (let i = 1; i < mass.length; i++) {
    currentNumber = mass[i].charCodeAt(0);
    if (currentNumber >= currentMax) {
      currentMax = currentNumber;
      rightOneInd = i;
    }
    if (currentNumber <= currentMin && currentNumber !== 48) {
      currentMin = currentNumber;
      rightTwoInd = i;
    }
  }

  for (let i = 0; i < rightOneInd; i++) {
    currentNumber = mass[i].charCodeAt(0);
    if (currentNumber < currentMax) {
      leftOneInd = i;
      break;
    }
  }

  currentNumber = mass[0].charCodeAt(0);
  if (currentNumber > currentMin) {
      leftTwoInd = 0;
  }
  else {
    rightTwoInd = reFindMin(mass, rightTwoInd);
    for (let i = 1; i < rightTwoInd; i++) {
      currentNumber = mass[i].charCodeAt(0);
      if (currentNumber > mass[rightTwoInd].charCodeAt(0)) {
        leftTwoInd = i;
        break;
      }
    }
  }
  return [leftOneInd, rightOneInd, leftTwoInd, rightTwoInd];
}

function spaceShip(mass: string) {
  const indexes: number[] = findMaxMin(mass);
  const leftOneInd: number = indexes[0];
  const rightOneInd: number = indexes[1];
  const leftTwoInd: number = indexes[2];
  const rightTwoInd: number = indexes[3];
  let out: string = "";
  let outMax: string;
  let outMin: string;
  let aux: any;

  if (leftOneInd !== -1 && rightOneInd !== -1) {
    aux = mass[leftOneInd];
    outMax = setCharAt(mass, leftOneInd, mass[rightOneInd]);
    outMax = setCharAt(outMax, rightOneInd, aux) + " ";
  }
  else {
    outMax = mass + " ";
  }

  if (leftTwoInd !== -1 && rightTwoInd !== -1) {
    aux = mass[leftTwoInd];
    outMin = setCharAt(mass, leftTwoInd, mass[rightTwoInd]);
    outMin = setCharAt(outMin, rightTwoInd, aux) + " ";
  }
  else {
    outMin = mass + " ";
  }
  out += outMin + outMax;
  process.stdout.write(out);
}

function readData(dat: string) {
  const lines: string[] = dat.split("\n");
  for (let i = 1; i < lines.length - 1; i++) {
    spaceShip(lines[i]);
  }
}

function main() {
  return fs.readFile(filePath, "utf8", (iss, dat) => {
    readData(dat);
  });
}

main();

/*
$ node neds
16F1EB8F838DC629365CA7FA3 F6F1EB8F838DC6293651A7CA3 27422082B7DA66EF4A
F742208227DA66EB4A 249D9B80A74EDB3FE48 F49D9B80A24EDB37E48 1FE664F58ACFE
FFE164F58AC6E 17068A8C967C56DE77 E7068A8C967C561D77 184A8A32626B08B606FCF54CBBD9
F84A8A32621B08B606FC654CBBD9 278DB55558B8979 D785B25558B8979 1F38F0845400392
FF3830845400192 103CD8121DAB43D731A DA3CD8121DAB4317310 1886C86E294347F2
F886C86E29434721 142EEC32AA6BA E41E2C32AA6BA 13D309AEB32E89E99FEBEC7
F31309AEB32E89E99DEBEC7 2FC5D7EAA0DB979 FEC5D72AA0DB979 1B4352DF2BAB823740D
FB4352DB2BA1823740D 1CC2CAD627BD208 DCC2CAD627B8201 1F75B3743AC7A992665203
F375B1743AC7A992665203 1A40C42571CDDD413ED897 EA40C42571CDDD4113D897
1D810948CFCCCCF FD810948CFCCC1C 20D5EAA0B9E495 E0D5EAA0B99425
1300395B54820A1122E8E EE00395B54820A1122381
*/
