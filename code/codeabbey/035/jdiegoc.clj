; $ lein check
;   Compiling namespace jdiego10.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting jdiego10.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns jdiego10.clj
  (:gen-class)
)

(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
          t  (count c)
          s  (get c 0)
          r  (get c 1)
          p  (get c 2)]
          (loop [i 0]
          (if (<s r)
            s=s+(s*p/100)
            t=t+1
          )
        )
        (print (t))
      )
    )
  )
)

(defn -main [& args]
  (process_file "DATA.lst")
)

; $clojure  jdiegog.clj
; 9 31 8 8 10 26 7 43 8 66 7 100 37 48 6 17 10 8
