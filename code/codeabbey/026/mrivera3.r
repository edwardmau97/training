# $ lintr::lint('mrivera3.r')


commdiv <- function() {
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  for (i in seq_len(nrow(data))) {
    a <- data$X1[i]
    b <- data$X2[i]
    while (a != b) {
      if (a < b) {
        b <- b - a
      }
      else if (b < a) {
        a <- a - b
      }
    }
    lcd <- data$X1[i] * data$X2[i] / a
    cat("(", a, lcd, ")")
  }
}
commdiv()

# $ Rscript mrivera3.r
# (30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
# (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641) (1 10)
# (30 48180) (160 184800) (2 40)
