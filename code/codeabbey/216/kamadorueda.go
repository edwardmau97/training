/*
 * $ golint -set_exit_status code/codeabbey/216/kamadorueda.go
 * $ echo $?
 *   0
 */

package main

import (
  "bufio"
  "fmt"
  "math/big"
  "os"
  "strings"
)

func main() {

  var numCurrentTestCase big.Int
  var numTestCases big.Int
  var results []string
  var scanner *bufio.Scanner
  var tokens [3]big.Int
  var tokensStr []string

  scanner = bufio.NewScanner(os.Stdin)

  // Advance the scanner one line and get the number of tests cases
  scanner.Scan()
  numTestCases.SetString(scanner.Text(), 10)
  numCurrentTestCase.SetInt64(0)

  // Instantiate a list to store the results
  results = make([]string, 0, numTestCases.Uint64())

  // Solve every tests case
  for numCurrentTestCase.Cmp(&numTestCases) == -1 {
    numCurrentTestCase.Add(&numCurrentTestCase, big.NewInt(1))

    // Advance the scanner one line and get 3 sequential LCG outputs
    scanner.Scan()
    tokensStr = strings.Fields(scanner.Text())

    // Cast them to numeric objects
    for index, element := range tokensStr {
      tokens[index].SetString(element, 10)
    }

    // Problem to solve:
    //   t2 = A * t1 + C (mod 2**64)
    //   t1 = A * t0 + C (mod 2**64)

    var A big.Int
    var C big.Int
    var M big.Int
    var R big.Int
    var temp1 big.Int
    var temp2 big.Int

    // Compute the modulus
    //   M = pow(2, 64)
    M.Exp(
      big.NewInt(2),
      big.NewInt(64),
      nil)

    // Compute A
    //   A = (t2 - t1) * modinv(t1 - t0)
    A.Mul(
      temp1.Sub(
        &tokens[2],
        &tokens[1]),
      temp2.ModInverse(
        A.Sub(
          &tokens[1],
          &tokens[0]),
        &M))

    // Compute C
    //   C = (t1 - A * t0)
    C.Sub(
      &tokens[1],
      temp1.Mul(
        &A,
        &tokens[0]))

    // Compute the next random value
    //   R = A * t2 + C (mod 2**64)
    R.Mod(
      R.Add(
        R.Mul(
          &A,
          &tokens[2]),
        &C),
      &M)

    results = append(results, R.String())
  }

  fmt.Println(strings.Join(results, " "))
}

/*
 * $ cat code/codeabbey/216/DATA.lst \
 *     | go run code/codeabbey/216/kamadorueda.go
 *   17168768104129676748 12983789159380637769 7989318077291446203
 *   13195145911395951001 2553393721488606865 13958165887772911120
 *   16458256830172631543 1446743428283962637 14001069139930511375
 *   14275449315735402846 3790233238010333260 17669067267533365044
 *   17529357248758541549 3817327874329108358 7447005828708649079
 *   17848711159704464844 7828534594901702269 3995457604873658977
 *   7093937703941312317 6725587951482251967 1278048703995008675
 *   4943466300170580738 999334567006702906 14783028198123730772
 *   918355663406936262 9870633452175593388 5582148297617089600
 *   8443533942284946455 12745539990463334053 11654804222213958204
 *   10781185282394367064
 */
