# $ lintr::lint('mrivera3.r')


formbmi <- function() {
"assigning height and weight to BMI grade"
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    values <- data[i, ]
    weight <- values[1]
    height <- values[2]
    bmi <- weight / height^2
    grade <- 0
    if (bmi <= 18.5) {
      grade <- "under";
    }
    else if (bmi <= 25) {
      grade <- "normal";
    }
    else if (bmi <= 30) {
      grade <- "over";
    }
    else {
      grade <- "obese";
    }
    results <- c(results, grade)
  }
  print(results, quote = FALSE)
}
formbmi()

# $ Rscript mrivera3.R
# over   normal under  obese  obese  obese  obese  over   under
# normal normal under  normal normal obese  normal obese  under
# normal obese  obese  normal obese  under  obese  under  under
# obese  obese  obese  under  obese  normal normal under
