###
 $ coffeelint mrsossa.coffee
###

fs = require 'fs'
l = 0;
do ->
  for line in fs.readFileSync("DATA.lst").toString().split '\n'
    if (l > 0)
      token = line.split(" ");
      weight = token[0];
      height = token[1];
      BMI = weight / (height*height);
      if(BMI < 18.5)
        console.log("under ");
      else if (BMI < 25.0 && BMI>=18.5)
        console.log("normal ");
      else if (BMI < 30.0 && BMI>=25.0)
        console.log("over ");
      else if (BMI >= 30.0)
        console.log("obese ");
    else
      l = 1;

###
 $ coffee mrsossa.coffee
 over normal under obese obese obese obese over under normal normal
 under normal normal obese normal obese under normal obese obese
 normal obese under obese under under obese obese obese under obese
 normal normal under
###
