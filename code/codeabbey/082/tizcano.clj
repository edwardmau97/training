(comment "
$ lein eastwood
== Eastwood 0.2.8 Clojure 1.8.0 JVM 10.0.2
Directories scanned for source files:
  src
== Linting tizcano.core ==
== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0")

(ns tizcano.core
  (:gen-class)
  (:require [clojure.string :as str]))

(def levenshtein
  (memoize
   (fn [v]
     (if (or (<= (count (nth v 0)) 0) (<= (count (nth v 1)) 0))
       (+ (count (nth v 0)) (count (nth v 1)))
       (if (= (last (nth v 0)) (last (nth v 1)))
         (levenshtein (vector (butlast (nth v 0)) (butlast (nth v 1))))
         (min
          (+ 1 (levenshtein (vector (butlast (nth v 0)) (nth v 1))))
          (+ 1 (levenshtein (vector (nth v 0) (butlast (nth v 1)))))
          (+ 1 (levenshtein (vector (butlast (nth v 0)) (butlast (nth v 1))))))
         )))))

(defn read-inputs
  [^long numbers]
  (print (levenshtein (str/split (read-line) #" ")) "")
  (when (> numbers 1)
    (read-inputs (- numbers 1))))

(defn -main
  [& args]
  (read-inputs (Long/parseLong (read-line))))

(comment "
  $ cat DATA.lst | lein run
output:
6 12 8 7 7 2 11 6 5 4 8 9 4 7 9 7 7 5 4 5 3 4 5 9 6")
