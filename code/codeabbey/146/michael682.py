#!/usr/bin/env python

# $ pylint michael682.py
# ------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 9.86/10, +0.14)
""" This script calculate the points of the color cube
    according to the movements of the user.
"""

import numpy as np


def getinputs():
    """Get the inputs from the txt file"""
    with open("DATA.lst", "r") as input_data:
        text = input_data.read()
    matrix = text.split("\n")
    cubei = matrix[1:int(matrix[0])+1]
    movementsi = matrix[len(matrix)-2].split(", ")
    cubei = [list(cubei[n]) for n, _ in enumerate(cubei)]
    cubei = np.matrix(cubei)
    return cubei, movementsi


def movecolumn(columnt):
    """Move one colum in gravity direction"""
    size = (np.size(columnt))
    columnt = (columnt[columnt != '-'])
    if np.size(columnt) == 0:
        return columnt
    while size > np.size(columnt):
        columnt = np.append('-', columnt)
    return columnt.reshape(size, 1)


def adjacentcells(yaxis, xaxis, maxv, cube, val):
    """ Change to - every value adjacent to the select cell
    """

    cube[yaxis, xaxis] = "-"

    if maxv > yaxis + 1:
        if cube[yaxis + 1, xaxis] == val:
            cube = adjacentcells(yaxis + 1, xaxis, maxv, cube, val)
    if yaxis - 1 > -1:
        if cube[yaxis - 1, xaxis] == val:
            cube = adjacentcells(yaxis - 1, xaxis, maxv, cube, val)
    if maxv > xaxis + 1:
        if cube[yaxis, xaxis + 1] == val:
            cube = adjacentcells(yaxis, xaxis + 1, maxv, cube, val)
    if xaxis - 1 > -1:
        if cube[yaxis, xaxis - 1] == val:
            cube = adjacentcells(yaxis, xaxis - 1, maxv, cube, val)

    return cube


def gravitydown(maxv, cubep, emptyc):
    """ organize the columns
    """
    removec = []
    for k in range(0, maxv):
        column = movecolumn(cubep[:, k])
        if np.size(column) == 0:
            removec = np.append(removec, k)
        else:
            cubep[:, k] = column

    for k in range(len(removec)-1, -1, -1):

        cubep = np.delete(cubep, int(removec[k]), 1)
        cubeaux = np.matrix(np.empty([maxv, maxv], dtype=object))
        cubeaux[:, :-1] = cubep
        emptyc = emptyc.reshape(maxv, 1)
        cubeaux[:, -1] = emptyc
        cubep = cubeaux
    return cubep


def calculatepoints(cubep, movementsp):
    """ Calculate the points of every movement
    """
    count = 0
    points = 0
    maxv = len(cubep)
    emptyc = []
    while maxv > np.size(emptyc):
        emptyc = np.append('-', emptyc)
    for mov in movementsp:
        mov = mov.split(" ")
        axisx = int(mov[1])
        axisx = maxv-axisx-1
        axisy = int(mov[0])
        row = cubep[axisx, :]
        val = row[0, axisy]
        cubep = adjacentcells(axisx, axisy, maxv, cubep, val)
        cubep = gravitydown(maxv, cubep, emptyc)
        countaux = count
        count = (np.count_nonzero(cubep == '-'))-count
        points = count*(count+1)/2+points
        count = count+countaux
    return int(points)


CUBE, MOVEMENT = getinputs()

print(calculatepoints(CUBE, MOVEMENT))
# $ python michael682.py
# 1161
