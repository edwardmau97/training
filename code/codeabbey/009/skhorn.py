#!/usr/bin/env python3
"""
Problem #9 Triangles
"""
class Triangles:
    """
    According to https://es.wikipedia.org/wiki/Tri%C3%A1ngulo
    there is a triangle clasification which can measure it's quality
    """
    triplet = []

    def __init__(self):

        while True:

            line = input()
            if line:

                if len(line) < 3:
                    pass
                else:
                    triangle_values = line.split()
                    self.triangle_builder(*triangle_values)
            else:
                break

        text = ' '.join(self.triplet)
        print(text)

    def triangle_builder(self, *args):
        """
        Fn to calculate the output of the given formula,
        quality = ((a+b-c)*(b+c-a)*(c+a-b))/(a*b*c)
        we will measure the triangles quality

        Args:
            *args       (qarg): data

        Returns:
            Append in triplet array the following value:
                - 1 if quality is good
                - 0 if it has no quality
        """
        a = int(args[0])
        b = int(args[1])
        c = int(args[2])
        quality = ((a+b-c)*(b+c-a)*(c+a-b))/(a*b*c)

        if quality >= 0:
            self.triplet.append("1")
        else:
            self.triplet.append("0")

Triangles()
