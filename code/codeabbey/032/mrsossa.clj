; $ lein check
;   Compiling namespace mrsossa.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting mrsossa.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns mrsossa.clj
  (:gen-class))

(defn stoi
  ([string]
   (map #(Integer/parseInt %)
        (clojure.string/split string #" ")
        )
   )
  )

(defn process_file
  (def sum 0)
  ([file]
   (with-open [rdr (clojure.java.io/reader file)]
     (doseq [line (line-seq rdr)]
       (let [l (stoi line)
             v (into [] l)]
         (let [n (get v 0)
               k (get v 1)
               i (set 0)
               res (set 0)
               (while (< i n)
                 (do
                   res (% (+ res k) i)
                   i (+ i 1)
                   )
                 )
               ]
           (print (str res+1))
           )
         )
       )
     )
   )
  )

(defn -main
  ([& args]
   (process_file "DATA.lst")
   (println)
   )
  )

; $ lein run
; 54
