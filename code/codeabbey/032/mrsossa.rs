/*
$ rustup run nightly cargo clippy
Compiling mrsossa.rs
$ rustc mrsossa.rs
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("DATA.lst").unwrap();
    let reader = BufReader::new(file);

    for lines in reader.lines() {
        let line = lines.unwrap();
        let token:Vec<&str> = line.split(" ").collect();
        let n:i32 = token[0].parse::<i32>().unwrap();
        let k:i32 = token[1].parse::<i32>().unwrap();
        let mut res:i32 = 0;
        let mut i:i32 = 1;
        while i <= n {
            res = (res + k) % i;
            i = i + 1;
        }
        println!("{}",res);
    }
}

/*
 $ rustc mrsossa.rs
 $ ./mrsossa
 53
*/
