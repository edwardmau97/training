# install.packages('lintr')
# lintr::lint('/home/juanmusic1/training_1/code/codeabbey/029/juanmusic1.R')


bubblesort <- function(vector) {
  item_count <- length(vector)
  index <- c()
  repeat {
    has_changed <- FALSE
    item_count <- item_count - 1
    for (i in 1:item_count) {
      if (vector[i] > vector[i + 1]) {
        t <- vector[i]
        vector[i] <- vector[i + 1]
        vector[i + 1] <- t
        has_changed <- TRUE
        index[i] <- i
      }
    }
    if (!has_changed) {
        break;
    }
  }
  vector
}

read_file <- function(file) {
    data <- readLines(file)
    num <- data[1]
    num <- strtoi(num)
    data <- data[2]
    data <- unlist(strsplit(data, " "))
    vector <- c()
    for (i in seq_len(length(data))) {
        vector[i] <- strtoi(data[i])
    }
    ret <- list(num, vector)
    ret
}

main <- function() {
    file <- "DATA.lst"
    data <- read_file(file)
    n <- unlist(data[1])
    data <- unlist(data[2])
    sorted <- bubblesort(data)
    pos <- c()
    for (i in 1:n) {
            cont <- 0;
            for (j in 1:n) {
                if (sorted[i] != data[j]) {
                   cont <- cont + 1
                } else {
                    break;
                }
            }
            pos[i] <- cont + 1
        }
        print(pos)
}

main()

# Rscript juanmusic1.R
# [1]  1  2  4  3  6 20 11 19 16 17  5 14  8 12 15  9 10 13  7 18
