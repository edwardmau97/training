'''
$mix credo --strict badwolf10.exs #linting
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.2 seconds (0.01s to load, 0.2s running checks)
7 mods/funs, found no issues.

$mix compile badwolf10.exs #compilation
'''

defmodule Main do
@moduledoc """
Main Module
"""
  def getentropy(symocc) do
    probs = getprobabilities(symocc, [], getsum(symocc, 0.0))
    computeentropy(probs, 0)
  end
  def computeentropy(plist, entropy) do
    if Enum.empty?(plist) do
      entropy
    else
      pi = List.first(plist)
      new_entropy = entropy - pi * :math.log2(pi)
      computeentropy(List.delete_at(plist, 0), new_entropy)
    end
  end
  def getprobabilities(symocc, problist, sum) do
    if Map.size(symocc) == 0 do
      problist
    else
      pkey = List.last(Map.keys(symocc))
      cprob = symocc[pkey] / sum
      new_problist = [cprob] ++ problist
      getprobabilities(Map.delete(symocc,pkey), new_problist, sum)
    end
  end
  def getsum(symocc, sum) do
    if Map.size(symocc) == 0 do
      sum
    else
      ckey = hd(Map.keys(symocc))
      usum = sum + symocc[ckey]
      getsum(Map.delete(symocc,ckey), usum)
    end
  end
  def procline(line) do
    charmap(line, %{}, 0)
  end
  def charmap(line, chmap, iterate) do
    if iterate > String.length(line) - 1 do
      chmap
    else
      keychar = String.to_atom(String.at(line, iterate))
      if chmap[keychar] == nil do
        new_chmap = Map.put(chmap, keychar, 1)
        charmap(line, new_chmap, iterate + 1)
      else
        new_count = chmap[keychar] + 1
        new_chmap = Map.put(chmap, keychar, new_count)
        charmap(line, new_chmap, iterate + 1)
      end
    end
  end
end

{:ok, contents} = File.read("DATA.lst")
lines = String.split(contents, "\n", trim: true)
lines = List.delete_at(lines, 0)
Enum.each lines, fn line -> IO.puts Main.getentropy(Main.procline(line)) end

'''
$elixirc badwolf10.exc
2.774397470347699
3.9828566633247844
3.7034651896016464
3.3632869239960552
3.795088586397733
'''
