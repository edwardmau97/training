# $ruby-lint kergrau.rb
answer = ''
flag = false

File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    sum = 0
    sum2 = 0
    ave = 0
    ave2 = 0
    comission = 0
    unless flag
      flag = true
      next
    end

    line.split(' ')[1..-1].each do |num|
      sum = sum + num.to_f
    end
    ave = sum / (line.split(' ').length - 1)

    line.split(' ')[1..-1].each do |num2|
      sum2 = sum2 + (ave - num2.to_f) ** 2
      comission = comission + num2.to_f * 0.01
    end

    ave2 = sum2 / (line.split(' ').length - 1)
    desviation = Math.sqrt(ave2)
    comission = comission / (line.split(' ').length - 1)


    if desviation >= (comission * 4)
      answer << "#{line.split(' ')[0]} "
    end
  end
end
puts answer

# ruby kergrau.rb
# DALG FANT FLNT JOOG CKCL MYTH WOWY
# INSX IMIX SUGR ZEOD JABA MARU PNSN SLVR VDKL
