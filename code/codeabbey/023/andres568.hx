/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var result:String = "";
    var inputData:Array<String> = [];
    var temp:String = "";
    var swaps:Int = 0;
    var checksum:Float = 0.0;
    lines.pop();
    inputData = lines[0].split(" ");
    inputData.pop();

    for (i in 1...inputData.length) {
      if (Std.parseInt(inputData[i - 1]) > Std.parseInt(inputData[i])) {
        temp = inputData[i - 1];
        inputData[i - 1] = inputData[i];
        inputData[i] = temp;
        swaps += 1;
      }
    }

    for (i in 0...inputData.length) {
      checksum += Std.parseInt(inputData[i]);
      checksum *= 113;
      checksum %= 10000007;
    }

    result = swaps + " " + checksum;
    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:44: 41 1962385
**/
