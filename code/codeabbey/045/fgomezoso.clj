;>lein eastwood
;== Eastwood 0.3.7 Clojure 1.10.0 JVM 1.8.0_231 ==
;Directories scanned for source files:
;
;src test
;== Linting fgomezoso.core ==
;== Linting fgomezoso.core-test ==
;== Linting done in 4207 ms ==
;== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
;

(ns fgomezoso.core
  (:gen-class)
  (:require [clojure.java.io :as io])
  (:require [clojure.string :as str]))

(defn transform [nline]
  (into [] (map #(Integer/parseInt %) (str/split nline #" ")))
)

(defn trimArray [nvector]
  (into [] (map (fn [x] (if (> x 52) (mod x 52) x))  nvector))
)

(defn shuffleArray [cards nums]
  (loop [i 0
         shcards cards
        ]
    (if (= i 52)
      (println (str/join " " shcards))
      (recur
         (inc i)
         (let [j (nums i)]
          (assoc shcards i (shcards j) j (shcards i))
         )
      )
    )
  )
)

(defn -main [& args]
  (with-open [rdr (io/reader "DATA.lst")]
    (doseq [line (line-seq rdr)]
      (let [deck
            [
             "CA" "C2" "C3" "C4" "C5" "C6" "C7" "C8" "C9" "CT" "CJ" "CQ" "CK"
             "DA" "D2" "D3" "D4" "D5" "D6" "D7" "D8" "D9" "DT" "DJ" "DQ" "DK"
             "HA" "H2" "H3" "H4" "H5" "H6" "H7" "H8" "H9" "HT" "HJ" "HQ" "HK"
             "SA" "S2" "S3" "S4" "S5" "S6" "S7" "S8" "S9" "ST" "SJ" "SQ" "SK"
            ]
            integerVector (transform line)
            trimmedNumbers (trimArray integerVector)
            cardShuffle (shuffleArray deck trimmedNumbers)
           ]
           cardShuffle
      )
    )
  (println)
 )
)

;>lein run
;H7 HT D6 DT SA C4 C7 C2 CJ C3 DQ S9 D3 H5 CT H4 H9 D4 H6 S4 D2 DK H8 CK SQ
;S8 H3 C5 C8 D8 HK C9 DJ DA S5 CA ST S6 D7 SK D5 HQ H2 D9 S2 C6 S7 CQ SJ HA
;HJ S3
