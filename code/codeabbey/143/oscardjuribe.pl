# perl -Mstrict -Mdiagnostics -cw oscardjuribe.pl
# oscardjuribe.pl syntax OK

# Program to solve the challenge Extended Euclidean Algorithm from codeabbey


package Oscardjuribe;

use strict;
use warnings;
use Fatal qw(print);

our $VERSION = 1.0;


sub bezout_identity {
  # method to calculate the r (remainder) a-b (coeficients) values

  # get x parameter
  my ($X,$Y) = @_;

  # init previous and current value of S
  my $sprev = 1;
  my $scur  = 0;

  # init previos and current value of T
  my $tprev = 0;
  my $tcur  = 1;

  # init previous value of remainder
  my $rprev = 0;

  # init remainder
  my $r = 1;
  # init quotient
  my $q = 0;

  # check the divisor and swap if X was the first divisor
  my $reverse = 0;

  if ($Y < $X) {
    $reverse = 0;
  }
  else {
    $reverse = 1;
  }

  # iterate until remainder is 0
  while ($r != 0) {

    # check max value
    if ($Y < $X) {

      # calculate remainder and quotient
      $r = $X % $Y;
      $q = int($X / $Y);
      # reassign X, Y values
      $X = $Y;
      $Y = $r;

    }
    else {
      # calculate remainder and quotient
      $r = $Y % $X;
      $q = int($Y / $X);
      # reassign X, Y values
      $Y = $X;
      $X = $r;
    }

    # update snext and tnext values
    my $snext = $sprev - $q * $scur;
    my $tnext = $tprev - $q * $tcur;

    # reassign values for next iteration
    $sprev = $scur;
    $tprev = $tcur;
    $scur = $snext;
    $tcur = $tnext;

    # check it is last iteration to don't override the remainder value
    if ($r != 0) {
      # save last remainder
      $rprev = $r;
    }
  }

  # check the first divisor to know the order of the values
  if ($reverse == 0) {
    # return if Y was first divisor
    return "$rprev $sprev $tprev";

  }
  else {
    # return if X was first divisor
    return "$rprev $tprev $sprev";
  }
}

sub solve_challenge {
  # method to solve challenge after open file

  # open file as argument
  my ($fh) = @_;

  # var to read number of testcases
  my $testcases;
  # check for the first line
  my $count = 0;

  # empty string to store solution
  my $EMPTY = q{};
  my $sol = $EMPTY;

  # init value to store x and y
  my $X;
  my $Y;
  my $line;

  # iterate over lines
  while(<$fh>) {

    if ($count == 0) {
      $testcases = $_;
      $count = 1;
    }
    else {

      # read current line
      $line = $_;

      # using split() function
      my @spl = split q{ }, $line;

      # get X and Y value
      $X = int $spl[0];
      $Y = int $spl[1];

      # concat current answer to solution's string
      $sol = $sol . bezout_identity($X,$Y)."\n";
    }
  }
  # print solution
  print $sol;
  return 1;
}


# open and read data file
my $filename = 'DATA.lst';

# open file
my $error_close = open my $fh, '<', $filename;

# call solve method
solve_challenge($fh);

# close file
my $error = close $fh;


1;

# $ perl oscardjuribe.pl
# 1 14063 -2218
# 2 5887 -3806
# 2 7896 -2117
# 1 -20417 18383
# 5 -1026 217
# 1 -6282 7717
# 2 -7326 10667
# 1 -6359 3605
# 1 1766 -8881
# 1 -15726 20863
# 1 3413 -14794
# 1 -11671 13858
# 62 -110 39
# 4 6570 -1027
# 1 5287 -13837
# 2 1231 -854
# 4 -6333 8573
# 1 10804 -10361
# 2 -989 1787
# 4 4243 -2524
# 6 142 -55
# 2 -18283 18481
# 1 25963 -4113
# 1 23919 -39409
# 2 -8977 20391
# 11 -1375 2753
# 2 834 -929
# 1 -14279 21248
