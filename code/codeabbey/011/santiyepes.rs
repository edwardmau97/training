/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 1.86 secs
$ rustc santiyepes.rs
$
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut line: &str = &buff.unwrap();
        let mut array: Vec<&str> = line.split(' ').collect();
        let mut numbers: Vec<i64> = Vec::new();

        for x in &array {
            let mut string_number: String = x.to_string();
            let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
            numbers.push(number_conver);
        }
        if numbers.len() > 1 {
            let mut result: i64 = 0;
            let mut sum: i64 = numbers[0] * numbers[1] + numbers[2];
            while sum > 0 {
                result += sum % 10;
                sum /= 10;
            }
            print!("{} ", result);
        }
    }
}

/*
$ ./santiyepes
21 18 26 28 22 22 11 22 17 19 27 28 21 22
*/
