/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var lines:Array<String> = cont.split("\n");
    var result:String = "";
    var pageRate1:Float = 0.0;
    var pageRate2:Float = 0.0;
    var time1:Float = 0.0;
    var time2:Float = 0.0;
    var minTime:Float = 0.0;
    var x:Int = 0;
    var y:Int = 0;
    var n:Int = 0;
    var inputData:Array<String> = [];
    lines.shift();
    lines.pop();

    for (i in 0...lines.length) {
      inputData = lines[i].split(" ");
      x =  Std.parseInt(inputData[0]);
      y =  Std.parseInt(inputData[1]);
      n =  Std.parseInt(inputData[2]);
      pageRate1 = Math.max(x, y) / (x + y) * n;
      pageRate2 = Math.min(x, y) / (x + y) * n;
      time1 =  Math.ceil(pageRate1) * Math.min(x, y);
      time2 = Math.ceil(pageRate2) * Math.max(x, y);
      minTime = Math.min(time1, time2);
      result = result + Std.string(minTime) + " ";
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:45: 318244719 13158054 278833660 132435810 51272717 46729010
  8760609 107074208 309055713 33295378 322580664 359769725 332892224
  162592318 175123025
**/
