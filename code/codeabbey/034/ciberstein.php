<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('DATA.lst')) {
  $file = file_get_contents("./DATA.lst");
  $file = explode("\n", trim($file));

  for($i = 0; $i <= $file[0] ; $i++)
    $array[$i] = explode(" ", $file[$i]);

  for ($i = 1 ; $i <= $array[0][0] ; $i++) {
    $A = 0;
    $B = 100;

    do {
      $X = ($A + $B) / 2;
      $Y = $array[$i][0] * $X + $array[$i][1] * sqrt(pow($X, 3)) -
      $array[$i][2] * exp(-$X / 50);

      if ($Y<$array[$i][3])
        $A = $X;

      if ($Y > $array[$i][3])
        $B = $X;

    }while(abs($Y - $array[$i][3]) > 0.00000000001);
    echo "$X ";
  }
}
else
  echo 'Error DATA.lst not found'
/*
./ciberstein.php
30.974526402045 48.741526790269
*/
?>
