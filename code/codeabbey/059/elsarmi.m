clc
clear all
close all

msg_ini = 'Insert the number to guess of 4 digits: ';
msg_tries = 'Insert the number of tries for guessing the number: ';
msg_guess = 'Insert the number to gues of 4 digits: ';

numb = input(msg_ini);
tries = input(msg_tries);

res_numb  = num2str(numb) - '0';

for i = 1 : tries
    guess = input(msg_guess);
    g_split = num2str(guess) - '0';
    
    BullC = 0;
    CowC  = 0;
    res = [];
    for f = 1:1:4
        if(g_split(f) == res_numb(f))
            BullC = BullC + 1;
        end
    end
    for t = 1:1:4
    for g = 1:1:4
        if(t==g)
            continue
        else
            if (g_split(t) == res_numb(g))
                CowC = CowC + 1;
            end
        end
    end
    end
    
    res = [BullC CowC];
    disp('Format: Bulls-Cows')
    disp(res)
end
