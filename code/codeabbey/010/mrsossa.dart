/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      var cas2 = cas.split(" ");
      var x1 = int.parse(cas2[0]);
      var y1 = int.parse(cas2[1]);
      var x2 = int.parse(cas2[2]);
      var y2 = int.parse(cas2[3]);
      var r1,r2;
      r1 = (y2 - y1)/(x2-x1);
      r2 = y1 - (r1 * x1);
      print("("+r1.round().toString()+" "+r2.round().toString()+")");
    }
  });
}

/* $ dart mrsossa.dart
 * (-78 -834) (-48 -353) (95 621) (58 -495) (91 -681)
 (-60 254) (-10 430) (-32 -645) (-48 -855) (-21 -279)
 (-66 520) (-97 -212) (11 268) (-48 -699) (73 -977)
*/
