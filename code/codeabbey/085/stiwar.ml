print_string "ingrese la cantidad de estrellas y el angulo: \n";;
let data = ref (read_line ());;
let dataList = List.map int_of_string(Str.split (Str.regexp " ") !data);;
let stars = (List.nth dataList 0);;
let angle = (List.nth dataList 1);;
let cont = Array.make (stars*3) "no";;
let n = ref 0;;
let x = ref 1;;
let y = ref 2;;
for i = 0 to (stars - 1) do
  print_string "ingrese nombre de la estrella, X y Y separados por un espacio \n";
  let dat = read_line () in
    let dList = (Str.split (Str.regexp " ") dat) in          
      cont.(!n) <- (List.nth dList 0);
      cont.(!x) <- (List.nth dList 1);
      cont.(!y) <- (List.nth dList 2);      
  n := !n + 3;
  x := !x + 3;
  y := !y + 3;
done;;
x := 1;;
y := 2;;
let frnd f = floor (f +. 0.5);;
let rad_to_deg ang = (
  ((float ang) *. 3.1416) /. 180.0;
);;
let t1 = ref 0.0;;
let t2 = ref 0.0;;
let t3 = ref 0;;
let t4 = ref 0;;
for k=0 to (stars - 1) do         
       t1 := ( (float_of_string cont.(!x)) *. (cos (rad_to_deg angle)) ) -. ( (float_of_string cont.(!y)) *. (sin (rad_to_deg angle)) );
       t2 := ( (float_of_string cont.(!x)) *. (sin (rad_to_deg angle)) ) +. ( (float_of_string cont.(!y)) *. (cos (rad_to_deg angle)) );
       t3 := int_of_float (frnd (!t1));
       t4 := int_of_float (frnd (!t2));
       cont.(!x) <- string_of_int !t3;
       cont.(!y) <- string_of_int !t4;       
   x := !x + 3;
   y := !y + 3;
done;;
let dataN = Array.make (stars) "";;
let dataY = Array.make (stars) "";;
let out = Array.make stars "n";;
n:=0;
y:=2;
for k=0 to (stars - 1) do
  dataN.(k) <- cont.(!n);  
  dataY.(k) <- cont.(!y);
  n := !n + 3;  
  y := !y + 3;
done;;
let posMin = ref 0;;
let ind = ref 0;;
let max = ref (int_of_string dataY.(0));;
for i=0 to ((Array.length dataY) - 1) do
  if(!max < int_of_string (dataY.(i)) )then(
    max := int_of_string ( dataY.(i));
  )
done;;
let minValue = ref !max;;
for k=0 to (stars - 1) do    
    for i=0 to (stars - 1) do      
      if(dataY.(i) <> "*")then(
        if( !minValue > (int_of_string (dataY.(i)) ))then(          
            posMin := i;
            minValue := (int_of_string (dataY.(i)));
        ) 
      )
    done;    
      dataY.(!posMin) <- "*";      
      out.(!ind) <- dataN.(!posMin);
      ind := !ind + 1;        
    for i=0 to ((Array.length dataY) - 1) do (*volver a capturar el max*)
      if(dataY.(i) <> "*")then(
        if(!max < int_of_string (dataY.(i)) )then(
          max := int_of_string ( dataY.(i));
        )
      )
    done;
    minValue := !max;
done;;
for i=0 to (stars - 1) do
  if(dataY.(i) <> "*")then(
    out.(stars - 1) <- dataN.(i);
  )
done;;
for k=0 to ((Array.length out) - 1) do
   Printf.printf "%s " out.(k);
done;;
