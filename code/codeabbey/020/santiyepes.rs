/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 0.86 secs
$ rustc santiyepes.rs
$
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut line: &str = &buff.unwrap();
        let mut array: Vec<&str> = line.split('-').collect();
        let mut count = 0;

        for x in &array {
            if x.chars().count() > 3 {
                for j in 0..65 {
                    if x.chars().nth(j) == Some('a')
                        || x.chars().nth(j) == Some('e')
                        || x.chars().nth(j) == Some('i')
                        || x.chars().nth(j) == Some('o')
                        || x.chars().nth(j) == Some('y')
                        || x.chars().nth(j) == Some('u')
                    {
                        count += 1;
                    }
                }
                print!("{} ", count);
            }
        }
    }
}

/*
$ ./santiyepes
10 11 15 14 17 11 10 12 6 6 5 8 11 10 16 9
*/
