# $ lintr::lint('mrivera3.r')


vowelcounter <- function() {
"counting the vowels on words"
  data <- read_csv("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    input <- as.character(data[i, ])
    split <- strsplit(input, "")[[1]]
    v <- c("a", "e", "i", "o", "u", "y")
    counter <- 0
    for (i in seq_len(length(split))) {
      vinput <- split[i] %in% v
      if (vinput == "TRUE") {
        counter <- counter + 1
      }
    }
    results <- c(results, counter)
  }
  return(results)
}
vowelcounter()

# $ Rscript mrivera3.r
#10 11 15 14 17 11 10 12  6  6  5  8 11 10 16  9
