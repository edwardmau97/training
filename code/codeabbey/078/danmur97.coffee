###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

class Point
  constructor: (@x,@y)->
  @add: (p1,p2)->
    new Point p1.x+p2.x,p1.y+p2.y
  @subtraction: (p1,p2)->
    new Point p1.x-p2.x,p1.y-p2.y
  trace: ()->
    console.log('Point:'+@x+','+@y)
  get: ()->
    r = [@x,@y]
    r

class Vector
  constructor: (@magnitude,@angle)->
  @from_point: (p)->
    magnitude = Math.sqrt(Math.pow(p.x,2)+Math.pow(p.y,2))
    angle = Math.atan2(p.y,p.x)
    new Vector magnitude,angle
  @scalar_mult: (vect,x)->
    new Vector vect.magnitude*x,vect.angle
  @add: (v1,v2)->
    r = Point.add(v1.to_point(),v2.to_point())
    v = new Vector 0,0
    Vector.from_point(r)
  @subtraction: (v1,v2)->
    r = Point.subtraction(v1.to_point(),v2.to_point())
    v = new Vector 0,0
    Vector.from_point(r)
  to_point: ()->
    new Point @magnitude*Math.cos(@angle),@magnitude*Math.sin(@angle)
  trace: ()->
    console.log('Vector:'+@magnitude+','+@angle)

class Line
  constructor: (@p1,@p2)->
    v1 = Vector.from_point(@p1)
    v2 = Vector.from_point(@p2)
    @unit_vector = new Vector 0,0
    @unit_vector = Vector.subtraction(v2,v1)
  to_global_coords: (x)->
    tmp = (Vector.scalar_mult(@unit_vector,x)).to_point()
    Point.add(@p1,tmp)
  trace:()->
    console.log('Line:')
    @p1.trace()
    @p2.trace()
    @unit_vector.trace()

class PolyLine
  constructor:(@points)->
    @lines = []
    for i in [0...@points.length-1]
      @lines[i] = new Line @points[i],@points[i+1]
  getPointsAtAlpha:(alpha)->
    points = []
    for i in [0...@lines.length]
      points[i] = @lines[i].to_global_coords(alpha)
    points

class BezierCurve
  constructor:(@points)->
  getPoint:(alpha)->
    tmp_poly = new PolyLine @points
    for i in [0...@points.length-2]
      tmp_poly = new PolyLine tmp_poly.getPointsAtAlpha(alpha)
    tmp_poly.getPointsAtAlpha(alpha)
  getCurvePoints:(alpha_step,N)->
    alpha = 0; i = 0; points = []
    while i < N
      tmp = this.getPoint(alpha)
      points[i] = this.getPoint(alpha)[0]
      alpha += alpha_step
      i++
    points

main = (x)->
  data = x[2]
  data = data.split "\r\n"
  meta = data[0].split " "
  M = meta[0];N = meta[1]
  points = []
  for i in [0...M]
    point = data[i+1].split " "
    points[i] = new Point parseInt(point[0]),parseInt(point[1])
  alphas = []
  # for i in [0...N]
  bcurv = new BezierCurve points
  result = ''
  curv_points = bcurv.getCurvePoints(1/(N-1),N)
  for i in [0...curv_points.length]
    p = curv_points[i].get()
    result += Math.round(p[0])+' '+Math.round(p[1])+' '
  console.log(result)
  0

main(process.argv)

###
$ coffee danmur97.coffee "$(< DATA.lst)"
494 492 458 504 429 515 407 525 391 534 382 542 377 548 377 554 382 559 390
564 402 567 417 570 434 573 452 574 473 575 494 576 515 575 537 574 558 573
577 570 596 567 612 562 625 556 636 549 643 541 646 532 644 520 638 507 626
492 607 475 583 455 551 433
###
