'''
$ pylint mrivera3.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.23/10, +0.77)
'''


def fng():
    "this function generates funny words"
    with open('DATA.lst', 'r') as file:
        input_1 = [int(number) for number in file.readline().split(" ")]
        input_2 = [int(number) for number in file.readline().split(" ")]
    x_initial = input_1[1]
    module = 2097152
    constant = 700001
    a_value = 445
    consonants = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p",
                  "r", "s", "t", "v", "w", "x", "z"]
    vowels = ["a", "e", "i", "o", "u"]
    res = []
    for k in input_2:
        values = []
        for i in range(k):
            xnext = (a_value * x_initial + constant) % module
            values.append(xnext)
            x_initial = xnext
        for i, r_new in enumerate(values):
            if i % 2 != 0:
                values[i] = vowels[r_new % 5]
            else:
                values[i] = consonants[r_new % 19]
        values = ''.join(values)
        res.append(values)
    return res


fng()


# $ python3 mrivera3.py
# hoj ladog xopim mipu direco sizucowo cebagebe kezuv jep cocov misu
# lovasaf pebolo fici teb vimok bejis dipanat hasekad cunig kukipada hehaho
