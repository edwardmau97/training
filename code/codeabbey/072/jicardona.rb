#!/usr/bin/env ruby
# $ ruby -wc jicardona.rb
# Syntax OK

total = seed = 0
testCases = []

File.open('DATA.lst') do |file|
  total, seed = file.readline.split
  testCases = file.readline.split
end

aValue = 445
cValue = 700001
period = 2097152

consonants = 'bcdfghjklmnprstvwxz'
vowels = 'aeiou'

solution = []

testCases.each do |testCase|
  word = ''
  for position in 1..testCase.to_i
    xNext = (aValue * seed.to_i + cValue) % period
    seed = xNext
    if position % 2 != 0
      word += consonants[xNext % 19]
    else
      word += vowels[xNext % 5]
    end
  end
  solution.push(word)
end

puts solution.join(' ')

# $ ruby jicardona.rb
# nom momese har damoveco gibapedo xuke kon lomu kaz
# hohu zahu koxihawa hila mocom fopumu dik jecaja
