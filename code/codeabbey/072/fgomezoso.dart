/*
> dartanalyzer fgomezoso.dart
Analyzing fgomezoso.dart...
No issues found!
*/
import 'dart:io';

void wordGenerator(params, lenList) {

  var totalWords = int.parse(params[0]);
  var seed = int.parse(params[1]);
  var A = 445;
  var C = 700001;
  var M = 2097152;
  var xNext = seed;
  var funnyList = '';

  for(var N = 0; N < totalWords; N++) {

    var amount = int.parse(lenList[N]);
    var funnyWord = '';

    for(var i = 0; i < amount; i++) {
      xNext =  (A * xNext + C) % M;

      if (i % 2 == 0) {
        var idx = xNext % 19;
        var char = getConsonant(idx);

        funnyWord = funnyWord + char;
      }
      else {
        var idx = xNext % 5;
        var char = getVowel(idx);

        funnyWord = funnyWord + char;
      }
    }

    if(N == totalWords - 1) {
      funnyList = funnyList + funnyWord;
    }
    else {
      funnyList = funnyList + funnyWord + ' ';
    }
  }

  print(funnyList);
}

String getConsonant(index) {
  var consonants = ['b', 'c', 'd', 'f', 'g',
                    'h', 'j', 'k', 'l', 'm',
                    'n', 'p', 'r', 's', 't',
                    'v', 'w', 'x', 'z'];

  var consCharacter = consonants[index];

  return consCharacter;
}

String getVowel(index) {
  var vowels = ['a', 'e', 'i', 'o', 'u' ];
  var vowelCharacter = vowels[index];

  return vowelCharacter;
}

void readFileByLines() {
  var file = File('DATA.lst');
  var lines = file.readAsLinesSync();

  var parameters = lines[0].split(' ');
  var lengths = lines[1].split(' ');

  wordGenerator(parameters, lengths);
}

void main(List<String> arguments) {
  readFileByLines();
}

/*
dart fgomezoso.dart
hoj ladog xopim mipu direco sizucowo cebagebe kezuv jep cocov
misu lovasaf pebolo fici teb vimok bejis dipanat hasekad cunig
kukipada hehaho
*/
