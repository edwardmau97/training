/*
$ golint slayfer1112.go
*/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

//Function to scan data
func dataEntry() [][]string {

  dataIn, _ := os.Open("Data.lst")
  dataScan := bufio.NewScanner(dataIn)
  var data [][]string
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      data = append(data, line)
    }
  }

  return data

}

//Global variables
var (
  A   int = 445
  C   int = 700001
  M   int = 2097152
  X0  int
  Nth int
)

//Function to make a map with a string
func mapping(toMap string) map[int]string {

  Map := make(map[int]string)
  letters := strings.Split(toMap, "")

  for i := 0; i < len(toMap); i++ {
    Map[i] = letters[i]
  }

  return Map

}

//Function that make the maps for consonants and vowels
func maps() (map[int]string, map[int]string) {

  C := "bcdfghjklmnprstvwxz"
  V := "aeiou"

  Cmap := mapping(C)
  Vmap := mapping(V)

  return Cmap, Vmap

}

//LinC = LinearCongruential function recommended in the challenge
func LinC(seed int, sizeWord int) ([]int, int) {

  X0 = seed

  var WordN []int

  for i := 1; i <= sizeWord; i++ {
    Nth = (A*X0 + C) % M
    X0 = Nth
    if i%2 != 0 {
      WordN = append(WordN, Nth%19)
    } else {
      WordN = append(WordN, Nth%5)
    }
  }

  return WordN, X0

}

//Function to print the results words
func printWord(random []int) {

  Cmap, Vmap := maps()

  var word string

  for i := 0; i < len(random); i++ {
    if (i+1)%2 != 0 {
      word += Cmap[random[i]]
    } else {
      word += Vmap[random[i]]
    }
  }

  fmt.Print(word + " ")

}

//Function main to run the code
func main() {

  data := dataEntry()

  words, _ := strconv.Atoi(data[0][0])
  X0, _ := strconv.Atoi(data[0][1])
  sizeWords := data[1]

  var Words [][]int

  for i := 0; i < words; i++ {
    sizeWord, _ := strconv.Atoi(sizeWords[i])
    var x []int
    x, X0 = LinC(X0, sizeWord)
    Words = append(Words, x)
  }

  for i := 0; i < len(Words); i++ {
    printWord(Words[i])
  }

}

/*
$ go run slayfer1112.go
ciz pesigemi kobex pate hedop cadehaco vajus madew honi dire
mop jisa fuha wim nakuco jumikuse zodadiz hetacudo biminez sib
*/
