#=
 $julia
 julia> using Lint
 julia> lintfile("jdiegoc.jl")
=#

open("DATA.lst") do file
  flag = false
  for ln in eachline(file)
      a=0
      b=0
      c=o
      if flag == false
         flag = true
         continue
      end
      a=parse(Int64)
      b=parse(Int64)
      c=parse(Int64)
      if ((a^2) + (b^2)) == (c^2)
         printl("R")
     elseif ((a^2) + (b^2)) < (c^2)
         printl("O")
     else
         printl("A")
      end
   end
end

#=
 $julia jdiegoc.jl
 O A A R R R R R O O A A O A R R R R O A
=#
