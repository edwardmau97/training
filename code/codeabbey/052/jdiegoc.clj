; $ lein check
;   Compiling namespace jdiegoc.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting jdiegoc.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

(ns jdiegoc.clj
  (:gen-class)
)

(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
          t  (count c)
          a  (get c 0)
          b  (get c 1)
          c  (get c 2)]
          (if (((a^2) + (b^2)) == (c^2))
             (print("R"))
            )
          (if (((a^2) + (b^2)) < (c^2))
             (print("O"))
            )
          (else)(
             (print("A"))
            )
        )
      )
    )
  )
)

(defn -main [& args]
  (process_file "DATA.lst")
)

; $clojure  jdiegoc.clj
; O A A R R R R R O O A A O A R R R R O A
