# $ lintr::lint('mrivera3.r')


dicerolling <- function() {
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    prov <- as.integer(data[i, ] * 6 + 1)
    results <- c(results, prov)
  }
  return(results)
}
dicerolling()

# $ Rscript mrivera3.r
#6 3 1 6 6 5 4 1 5 4 4 6 3 4 1 6 6 2 5 4 3 4 5 2
