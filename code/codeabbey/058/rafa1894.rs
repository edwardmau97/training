/*
$ rustfmt rafa1894.rs #linting
$ rustc rafa1894.rs #compilation
*/

use std::env;
use std::fs;

fn names(cs: &u32) -> (&str, &str) {
    let sv = cs / 13;
    let rv = cs % 13;
    let suits = vec!["Clubs", "Spades", "Diamonds", "Hearts"];
    let ranks = vec![
        "2", "3", "4", "5", "6", "7", "8", "9", "10",
        "Jack", "Queen", "King", "Ace",
    ];
    return (ranks[rv as usize], suits[sv as usize]);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file = &args[1];
    let inputstr = fs::read_to_string(file).expect("Error");
    let lines = inputstr.lines();
    let mut n;
    let mut x = 0;
    for i in lines {
        if x == 0 {
            x = 1;
            continue;
        }
        let numsts = i.split(' ');
        for j in numsts {
            n = j.parse::<u32>().unwrap();
            let (a, b) = names(&n);
            print!("{}-of-{} ", a, b);
        }
    }
}

/*
$ ./rafa1894 DATA.lst
King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
 Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
 Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
 3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
 2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
*/
