/*
➜  058 git:(sgomezatfluid) ✗ dartanalyzer sgomezatfluid.dart
Analyzing sgomezatfluid.dart...
No issues found!
*/

import 'dart:io';

void main() async {
  List<String> data = await new File('DATA.lst').readAsLines();
  List<String> numberstring = data[1].split(' ');
  List<int> numberlist = getNumbers(numberstring);
  List<String> suits = ['Clubs', 'Spades', 'Diamonds', 'Hearts'];
  List<String> ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10',
                        'Jack', 'Queen', 'King', 'Ace'];
  String answer = '';
  numberlist.forEach(
    (number) {
      int suit = (number/13).floor();
      int rank = (number%13);
      answer = answer + ranks[rank] + '-of-' + suits[suit] + ' ';
    }
  );
  print(answer);
}

List<int> getNumbers(List<String> numberstring) {
  List<int> numberlist = [];
  numberstring.forEach(
    (number) {
      numberlist.add(int.parse(number));
    }
  );
  return numberlist;
}

/*
➜  058 git:(sgomezatfluid) ✗ dart sgomezatfluid.dart
King-of-Hearts King-of-Clubs 4-of-Spades 4-of-Clubs Queen-of-Clubs
Jack-of-Diamonds Ace-of-Clubs 9-of-Diamonds 7-of-Clubs Queen-of-Hearts
Queen-of-Diamonds 7-of-Hearts 7-of-Diamonds Queen-of-Spades 6-of-Hearts
3-of-Hearts 9-of-Clubs 10-of-Diamonds 6-of-Diamonds 2-of-Clubs King-of-Spades
2-of-Hearts Ace-of-Diamonds Jack-of-Hearts 10-of-Clubs 5-of-Diamonds
*/
