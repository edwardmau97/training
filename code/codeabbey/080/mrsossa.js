/*
 $ eslint mrsossa.js
*/
/* global print */

import { readFileSync } from 'fs';

const dat = readFileSync('DATA.lst').toString().split('\n');
for (let ind = 0; ind < dat.length - 1; ind++) {
  if (ind !== 0) {
    const token = dat[ind].split(' ');
    const por = 100;
    const alan = Number(token[0]) / por;
    const bob = Number(token[1]) / por;
    const res = (alan / (alan + bob - (alan * bob))) * por;
    print(Math.round(res));
  }
}

/*
 $ js mrsossa.js
48 46 94 72 89 72 71 73 58 50 55 52 87 85
76 96 66 88 35 93 82 61 74 40
*/
