/*
$ rustfmt fgomezoso.rs
$ rustc fgomezoso.rs
*/

/*
This program sorts an array of numbers in non-decreasing order.
It returns the number of swaps made and the number of iterations.
*/

use std::fs;
use std::io;

fn bubble_sort(len: usize, nums: &mut Vec<i32>) {

    let mut not_sorted = 1;
    let mut total_swaps = 0;
    let mut total_passes = 0;

    while not_sorted == 1 {
        let mut j = 1;
        let mut swap_count = 0;

        for i in 0..len - 1 {
            if nums[i] > nums[j] {
                let aux = nums[j];
                nums[j] = nums[i];
                nums[i] = aux;

                swap_count += 1;
            }

            j += 1;
        }

        total_swaps += swap_count;
        total_passes += 1;

        if swap_count == 0 {
            not_sorted = 0;
        }
    }

    println!("{} {}", total_passes, total_swaps);
}

fn main() -> io::Result<()> {

    let file = fs::read_to_string("DATA.lst")?;
    let my_array: Vec<&str> = file.split("\r\n").collect();

    let array_length = my_array[0].parse::<usize>().unwrap();
    let my_numbers = my_array[1];
    let mut num_array = vec![];

    let vector: Vec<&str> = my_numbers.split(" ").collect();

    for x in &vector {
        num_array.push(x.parse::<i32>().unwrap());
    }

    bubble_sort(array_length, &mut num_array);

    Ok(())
}

/*
$ ./fgomezoso
16 97
*/
