/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function segment(content, startIndex, segPart) {
/* Segment of the line and set the value of the position for each sentences*/
  const pieceWord = content.slice(startIndex);
  const withPosition = pieceWord.concat(' ', startIndex);
  const firstPart = segPart.concat([ withPosition ]);
  if (pieceWord.length === 1) {
    return firstPart;
  }
  return segment(content, startIndex + 1, firstPart);
}

function sortedList(nextSeg, indexComp, segPartCreated) {
/* The sort of the list of sentences product of the segmentation*/
  const lastVal = segPartCreated.length - indexComp;
  if (lastVal === 0) {
    const firstPart = segPartCreated.slice(0, lastVal + 1);
    const newValue = [ nextSeg ];
    const secondValue = segPartCreated.slice(lastVal + 1,
      segPartCreated.length);
    const finalValue = firstPart.concat(newValue, secondValue);
    return finalValue;
  }
  if (nextSeg > segPartCreated[lastVal]) {
    const firstPart = segPartCreated.slice(0, lastVal + 1);
    const newValue = [ nextSeg ];
    const secondValue = segPartCreated.slice(lastVal + 1,
      segPartCreated.length);
    const finalValue = firstPart.concat(newValue, secondValue);
    return finalValue;
  }
  return sortedList(nextSeg, indexComp + 1, segPartCreated);
}

function reCall(segmentParts, startIndex, segPartCreated) {
/* Take each sentences and compares it with the sorted list that has been
save*/
  const nextSeg = segmentParts[startIndex];
  const indexComp = 1;
  const orderedSeg = sortedList(nextSeg, indexComp, segPartCreated);
  if (startIndex === segmentParts.length) {
    return orderedSeg;
  }
  return reCall(segmentParts, startIndex + 1, orderedSeg);
}

function location(element) {
/* After sorted the original position number of each sentence is taken with
this function*/
  const numFind = element.lastIndexOf(' ');
  const suffixNum = element.slice(numFind + 1);
  return suffixNum;
}

function suffixArrayCal(erro, contents) {
/* Set the initial values and calls every function to calculate the suffix
array for the text on the document*/
  if (erro) {
    return erro;
  }
  const content = String(contents.split('\n'));
  const startIndex = 0;
  const segPart = [];
  const segmentParts = segment(content, startIndex, segPart);
  const segPartCreated = [ '' ];
  const orderedSeg = reCall(segmentParts, startIndex, segPartCreated);
  const finalArray = orderedSeg.slice(2);
  const suffixNum = finalArray.map((element) => location(element));
  const suffixConc = suffixNum.join(' ');
  const output = process.stdout.write(`${ suffixConc } \n`);
  return output;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    suffixArrayCal(erro, contents));
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();
/**
node jenniferagve.js
input:INTERPRETING CREATES RATES
-------------------------------------------------------------------
output: 12 20 22 16 13 15 3 24 18 7 11 9 0 10 1 5 21 14 6 4 25 19 2 23 17 8
*/
