# $ lintr::lint('mrivera3.r')


sqroot <- function() {
  library(readr)
  data <- read_table2("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    r <- 1
    for (j in seq_len(data$X2[i])) {
      r <- (r + data$X1[i] / r) / 2
      r <- as.numeric(r)
    }
    results <- c(results, r)
  }
  return(results)
}
sqroot()

# $ Rscript mrivera3.r
# 7.416198    6.082763   22.068076  293.518824    5.477226   81.154174
# 59.674115   89.705072 2178.958321   27.531800   12.755078    9.110434
# 4.898979
