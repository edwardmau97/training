#!/usr/bin/env python3
"""
Problem #18 Square Root
"""
import math

class SquareRoot:

    total = []
    def __init__(self):

        while True:

            line = input()
            if line:

                if len(line) < 3:
                    pass
                else:
                    data = line.split()
                    self.square_root(*data)
            else:
                break
        text = ' '.join(self.total)
        print(text)

    def square_root(self, *data):
        X = int(data[0])
        N = int(data[1])
        r = 1
        if N == 0:
            r = 1
            self.total.append(str(r))
        else:
            while N > 0:
                d = X / r
                r = (r + d)/2
                N -= 1

            self.total.append(str(r))

SquareRoot()
