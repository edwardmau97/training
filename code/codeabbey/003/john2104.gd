# gdlint john2104.gd
# Success: no problems found


extends Node


func _ready():
    var input = return_data()
    var rows = input.split("\n")
    rows.remove(0)
    rows.remove(rows.size()-1)
    var results = []
    var output = ""
    for sum in rows:
        var array = sum.split(" ")
        var one = int(array[0])
        var two = int(array[1])
        results.append(one + two)
    for i in results:
        output += str(i)+" "
    print(output)

func return_data():
    var file = File.new()
    file.open("res://codeabbey_003/DATA.lst", File.READ)
    return file.get_as_text()

# Create a scene in godot and attach this script
# Run the scene
#
# ** Debug Process Started **
# OpenGL ES 3.0 Renderer: Mesa DRI Intel(R) HD Graphics 520 (Skylake GT2)
# 571514 1276856 379605 1167977 214749 1904562 1504121 1516719 971334 1462549
# 1407121 659593 314904
