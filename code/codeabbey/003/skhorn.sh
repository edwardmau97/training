#!/bin/bash
#
# Problem #3 Sums in Loop
#
while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" | wc -c)
    if [[ "$len" -gt 3 ]]
    then
        count=1
        total=0
        ARRAY=($line)
        for item in "${ARRAY[@]}"; do
            let total+=$item
        done
        output_array[$count]=$total
    fi
    printf '%s ' "${output_array[@]}"

done < "$1"
