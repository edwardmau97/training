/*
$ eslint jarboleda.js
$
*/
function getB(alpha, sum) {
  /* b = (sum(a - sum/2))/(a - sum) */
  return (sum * (alpha - (sum / 2))) / (alpha - sum);
}

const bigNumber = 100000000;
const theBigArray = new Array(bigNumber);
/* console.log(new Date().toLocaleTimeString()); */

function solveForLine(sum) {
  const filteredArray = theBigArray.slice(0, (sum / 2) + 1)
    .fill('a')
    .map((element, index) => (index + 1))
    .map((element) => ([ element, getB(element, sum) ]))
    .filter((value) => (Number.isInteger(value[1])))
    .map((value) =>
      (Math.sqrt((value[0] * value[0]) + (value[1] * value[1]))))
    .filter((value) => (Number.isInteger(value)));
  return filteredArray[0];
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const zero = 0;
  const numberOfLines = contents.split('\n')[zero];
  const splited = contents.split('\n').map((line) => (line.split(' ')))
    .slice(1, numberOfLines + 1);
  const answer = splited.map((line) => (solveForLine(line)))
    .map((value) => (value * value))
    .join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
88390026150409 41051455793881 42074487655225
107016728422321 100589526301489 65692078654225 31933179393025

*/
