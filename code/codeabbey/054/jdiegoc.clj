 (comment "
 $ lein check
   Compiling namespace jdiegoc.clj
 $ lein eastwood
   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
   Directories scanned for source files:
     src test
   == Linting jdiegoc.clj ==
   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
 ")

(ns jdiegoc.clj
  (:gen-class)
)

(defn s2c [wsstr]
  (into [] (map #(Double/parseDouble %) (clojure.string/split wsstr #" ")))
)

(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
          t (count c)
          n (get c 0)
          m (get c 1)
          a = (n*n) - (m*m)
          b = 2*n*m
          c = (n*n) + (m*m)
          abc = a + b + c
          (if (=== abc s2c)
           (print (c*c))
          )
        )
      )
    )
  )
)

(defn -main [& args]
  (process_file "DATA.lst")
)

(comment "
 $ clojure jdiegoc.clj
 88390026150409
 41051455793881
 42074487655225
 107016728422321
 100589526301489
 65692078654225
 31933179393025
")
