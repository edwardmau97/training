# gdlint john2104.gd
# Success: no problems found


extends Node


func _ready():
    var input = return_data()
    var rows = input.split("\n")
    rows.remove(0)
    rows.remove(rows.size()-1)
    var results = []
    var output = ""
    for sum in rows:
        var array = sum.split(" ")
        var one = int(array[0])
        var two = int(array[1])
        if(one > two):
           results.append(two)
        else:
           results.append(one)

    for i in results:
        output += str(i) + " "
    print(output)

func return_data():
    var file = File.new()
    file.open("DATA.lst", File.READ)
    return file.get_as_text()

# Create a scene in godot and attach this script
# Run the scene
#
# ** Debug Process Started **
# OpenGL ES 3.0 Renderer: Mesa DRI Intel(R) HD Graphics 520 (Skylake GT2)
# 3 2 15
