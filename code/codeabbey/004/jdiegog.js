/*
 $ eslint jdiego.js
*/
/* global print */

import * as file from 'file';

const dat = file.readFile('DATA.lst').toString().split('\n');
for (let i = 0; i < dat.length - 1; i++) {
  if (i !== 0) {
    const token = dat[i].split(' ');
    const nums = (Number(token[0]) < Number(token[1]));
    let res = 0;
    for (let cas = 0; cas < nums.toString().length; cas++) {
      res += Number(nums.toString().charAt(cas));
    }
    print(res);
  }
}

/*
$ js jdiego.js
2 6 1 4 6 0 1 2 6 2 2 0 4 2 3 6 0 5 1 5 5 4 5 6 4 1 4
*/
