/*
tslint jdiego.ts
jdiego10.ts file  pass linting
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
 const token = line.split(" ");

 if (token[0]<token[1]){
     console.log(token[0]);
 }
 else{
     console.log(token[1]);
   }
});
/*
 $ts jdiegoc.ts
 2 6 1 4 6 0 1 2 6 2 2 0 4 2 3 6 0 5 1 5 5 4 5 6 4 1 4
*/
