###
$coffeelint mmarulandc.coffee
###

fs = require 'fs'
config = 'DATA.lst'

readedFile = ->
  fs.readFileSync config, 'utf8'

process = ->
  result = ''
  reader = readedFile().split '\n'
  for index in [1...reader.length-1]
    pair = reader[index].split ' '
    result += pair[0] * (pair[1]-1) + " "
  console.log (result)

process()

###
$coffee mmarulandc.coffee
51 27 57 15 33 38 133 76 7
###
