/**
  linting
  $ haxelib run checkstyle -s Andres568.hx
  Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
  No issues found.
**/

using StringTools;

/**
  class with the main method
**/
class Andres568 {

  // Haxe applications have a static entry point called main
  static function main() {
    var cont:String = sys.io.File.getContent("DATA.lst");
    var formatedCont:String = StringTools.rtrim(cont.replace("\n", " "));
    var numbers:Array<String> = formatedCont.split(" ").slice(1);
    var index:Int = 0;
    var result:String = "";
    var firstNumber:Int = 0;
    var secondNumber:Int = 0;
    var thirdNumber:Int = 0;
    var arr:Array<Int> = [];

    while (index < numbers.length) {
      firstNumber = Std.parseInt(numbers[index]);
      secondNumber = Std.parseInt(numbers[index + 1]);
      thirdNumber = Std.parseInt(numbers[index + 2]);
      arr.push(firstNumber);
      arr.push(secondNumber);
      arr.push(thirdNumber);
      arr.sort(function(a, b) return a - b);
      result = result + arr[0] + " ";
      arr = [];
      index = index + 3;
    }

    trace(result);
  }
}
/**
  $ haxe -main Andres568 --interp
  Andres568.hx:40: 107814 386086 -5721210 -3265497 -9006366 124682 -455274
  -4695046 -2027624 -2726599 6289004 -2232700 -5564619 -9844812 -3110310
  -2335335 -9253696 -9053831 -3981282 -2784460 -1406274 -8164135 3309434
**/
