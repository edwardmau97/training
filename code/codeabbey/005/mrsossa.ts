/*
 $ tslint mrsossa.ts
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
rl.on("line", function(line) {
    const token = line.split(" ");
    if (+token[0] < +token[1] && +token[0] < +token[2]) {
        console.log(+token[0]);
    }
    else if (+token[1] < +token[0] && +token[1] < +token[2]) {
        console.log(+token[1]);
    }
    else if (+token[2] < +token[1] && +token[2] < +token[0]) {
        console.log(+token[2]);
    }
});

/*
 $ tsc mrsossa.ts
 $ node mrsossa.js
 107814 386086 -5721210 -3265497 -9006366 124682 -455274 -4695046 -2027624
 -2726599 6289004 -2232700 -5564619 -9844812 -3110310 -2335335 -9253696
 -9053831 -3981282 -2784460 -1406274 -8164135 3309434
*/
