"""
$ pylint calderonsin.py
Global evaluation
----------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def minimun():
    """Minimun function"""
    data = open('DATA.lst', 'r')
    number = int(data.readline())
    a_1 = [None] * number
    b_1 = [None] * number
    c_1 = [None] * number
    minimum = []
    for i in range(0, number):
        numbers = data.readline()
        intro = numbers.split()
        a_1[i] = int(intro[0])
        b_1[i] = int(intro[1])
        c_1[i] = int(intro[2])
        if a_1[i] < b_1[i]:
            if a_1[i] < c_1[i]:
                minimum.append(a_1[i])
            else:
                minimum.append(c_1[i])
        else:
            if b_1[i] < c_1[i]:
                minimum.append(b_1[i])
            else:
                minimum.append(c_1[i])
    for j in minimum:
        print(j, " ")


minimun()


# $ python calderonsin.py build #command
# 107814 386086 -5721210 -3265497 -9006366 124682 -455274
# -4695046 -2027624 -2726599 6289004 -2232700
# -5564619 -9844812 -3110310 -2335335 -9253696
# -9053831 -3981282 -2784460 -1406274 -8164135 3309434
