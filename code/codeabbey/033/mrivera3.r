# $ lintr::lint('mrivera3.r')


paritycontrol <- function() {
  data <- scan("DATA.lst")
  prov <- c()
  res <- c()
  for (i in data) {
    prov <- sum(as.binary(i))
    if (prov %% 2 == 0) {
      bin <- as.binary(i, n = 8)
      bin[1] <- 0
      res <- c(res, as.integer(bin))
    }
  }
  msg <- chr(res)
  msg <- paste(msg, collapse = "")
  print(noquote(msg))
}
paritycontrol()

# $ Rscript mrivera3.r
# 8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
