/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build631825641
*/
package main

import (
  "bufio"
  "fmt"
  "io"
  "os"
  "path/filepath"
  "strconv"
  "strings"
)

func main() {

  absPath, _ := filepath.Abs("DATA.lst")
  arr, _ := readFileToStr(absPath)
  nums := strings.Split(arr[0], " ")
  for i := 0; i < len(nums); i++ {
    v, _ := strconv.Atoi(nums[i])
    p := checkParity(v)
    if p%2 == 0 {
      fmt.Printf("%v", string(v&127))
    }
  }
  fmt.Printf("\n")
}

func checkParity(v int) int {
  cont := 0
  for i := 0; i < 8; i++ {
    if (v & 1) == 1 {
      cont++
    }
    v = v >> 1
  }
  return cont
}

func readFileToStr(fn string) (arr []string, err error) {

  //Save file to buffer and close
  file, err := os.Open(fn)
  defer file.Close()

  if err != nil {
    return nil, err
  }

  //Cread reader buffer from file
  reader := bufio.NewReader(file)

  //Start reading buffer
  var line string
  for {
    line, err = reader.ReadString('\n')
    //Delete \n from last character of string
    if last := len(line) - 1; last >= 0 && line[last] == '\n' {
      line = line[:last]
    }
    if line != "" {
      arr = append(arr, line)
    }
    if err != nil {
      break
    }
  }

  if err != io.EOF {
    fmt.Printf("ERROR: %v\n", err)
  }

  return arr, nil
}

/*
./lope391
8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
*/
