/*
$ eslint tizcano.js
$
*/

function auxNeighbors(graph, visited, start) {
  const text = graph.map((vertex) => {
    if (vertex.includes(start)) {
      if (vertex[0] === start) {
        if (!visited.includes(vertex[1])) {
          return vertex[1];
        }
      }
      if (vertex[1] === start) {
        if (!visited.includes(vertex[0])) {
          return vertex[0];
        }
      }
    }
    return false;
  });
  return text.filter((element) => element !== false);
}

function flatten(elements) {
  return elements.reduce(
    (result, current) =>
      result.concat(Array.isArray(current) ? flatten(current) : current),
    []
  );
}
function quickSort(arr) {
  if (arr.length) {
    return flatten([
      quickSort(
        arr.slice(1).filter((element) => element <= arr[0])
      ),
      arr[0],
      quickSort(
        arr.slice(1).filter((element) => element > arr[0])
      ),
    ]);
  }
  return [];
}
function compareMin(elem, pivot) {
  return elem.a <= pivot.a;
}
function compareMax(elem, pivot) {
  return pivot.a < elem.a;
}
function quickSortFn(arr, fnt, ftn) {
  if (arr.length) {
    return flatten([
      quickSortFn(
        arr.slice(1).filter((element) => fnt(element, arr[0])),
        fnt,
        ftn
      ),
      arr[0],
      quickSortFn(
        arr.slice(1).filter((element) => ftn(element, arr[0])),
        fnt,
        ftn
      ),
    ]);
  }
  return [];
}

function auxBfs(graph, visited = [], start, queue, order = []) {
  if (!queue.length) {
    return order;
  }
  const neighborsNonSorted = auxNeighbors(graph, visited, start);
  const neighbors = quickSort(neighborsNonSorted);
  const concatedQueue = [ ...queue ].slice(1).concat(neighbors);
  const test = neighbors.map((elem) => ({ a: elem, b: start }));
  const testo = order.concat(test);
  const newVisited = visited.concat(concatedQueue);
  return auxBfs(
    graph,
    newVisited.filter((element, pos) => newVisited.indexOf(element) === pos),
    concatedQueue[0],
    concatedQueue,
    testo
  );
}
function bfs(graph, start) {
  return auxBfs(graph, [ start ], start, [ start ], [ { a: 0, b: -1 } ]);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const graph = inputFile
    .slice(1)
    .map((element) => element.split(' ').map((elm) => parseInt(elm, 10)));
  const solvedObject = bfs(graph, 0, inputFile[0].split(' ')[0] - 1);
  const solvedObjectSorted = quickSortFn(solvedObject, compareMin, compareMax);
  const solvedArray = solvedObjectSorted.map((element) => element.b);
  const output = process.stdout.write(`${ solvedArray.join(' ') }\n`);
  return output;
}

const fileReader = require('fs');
function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node tizcano.js
output:
-1 11 29 11 33 25 7 10 33 25 0 0 28 11 22 41 29 32 28 25 29 20 24 11 0 10 28 1
10 0 25 28 11 10 2 32 29 33 1 16 1 10 25 29
*/
