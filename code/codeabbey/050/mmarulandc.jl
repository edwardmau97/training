#=
 $julia
 julia> using Lint
 julia> lintfile("mmarulandc.jl")
=#

function isPalin(string)
  newString = lowercase(replace(string,r"\W"=>""))
  if newString[1:length(newString)] == newString[length(newString):-1:1]
    print("Y", " ")
  else
    print("N", " ")
  end
end

file = open("DATA.lst")
lines = readlines(file)
tries = lines[1]
array = []
for i = 2: parse(Int,tries) + 1
  isPalin(lines[i])
end

# $ julia mmarulandc.jl
# N Y Y
