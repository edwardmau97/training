/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');

const results = [];

files.readFile(path.join(__dirname, 'DATA.lst'), (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const inputs = inputData.toString().split('\r\n');
  const ignoreChars = [ ' ', '?', '!', ',', '.', '-' ];

  for (let i = 1; i <= Number(inputs[0]); i++) {
    const arrLetters = inputs[i]
      .toLowerCase()
      .split('')
      .filter((letter) => !ignoreChars.includes(letter));
    const word = arrLetters.join('');
    const reverseWord = arrLetters.reverse().join('');
    if (reverseWord === word) {
      results.push('Y');
    }
    else {
      results.push('N');
    }
  }

  console.log(results.join(' '));
});

/*
$ node ngarci
output:
N Y Y
*/
