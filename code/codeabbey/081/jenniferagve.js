/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

/* eslint-disable fp/no-unused-expression*/

function onesCount(element) {
/* Here each one of the binary number is counted*/
  const segmentBin = element.split('');
  const numConvert = segmentBin.map((decimalNumb) => Number(decimalNumb));
  const totalCount = numConvert.reduce((sum, onesquant) => sum + onesquant);
  return totalCount;
}


function binInterpreter(erro, contents) {
/* Here each funtion is called to convert de decimal number to binary and
count the number of ones by each single value*/
  if (erro) {
    return erro;
  }
  const contentsInput = contents.split('\n');
  const inputData = String(contentsInput.slice(1));
  const numSelect = inputData.split(' ');
  const numConvert = numSelect.map((element) => Number(element));
  const binConvert = numConvert.map((element) => (element >>> 0).toString(2));
  const countEachNumber = binConvert.map((element) => onesCount(element));
  const countFormat = countEachNumber.join(' ');
  const output = process.stdout.write(`${ countFormat } `);
  return output;
}

/* eslint no-bitwise: ["error", { "allow": [">>>"] }] */
const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read all the data from the cards*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    binInterpreter(erro, contents));
}

fileLoad();

/**
node jenniferagve.js
input:3
1 100 -1
-------------------------------------------------------------------------
output:1 3 32
*/
