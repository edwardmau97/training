#!/usr/bin/python3
""" Codeabbey 40: Paths in the grid
Finds the number of paths in a grid that has blockades (pitfalls)
moving only down and to the right.
using recursion and dynamic programming.
*** Sample run with DATA.lst
$ python raballestasr.py
2076192
(with a 15x19 grid)
*** Linter output
$ pylint raballestasr.py
No config file found, using default configuration
************* Module pathfinder
C: 56, 0: Final newline missing (missing-final-newline)
Your code has been rated at 9.57/10 (previous run: 7.39/10, +2.17)
"""
#
MAZE = []

def count_paths(row, col):
    """ Computes number of paths from given node to end ($) recursively
    and stores them so they don't have to be recomputed when visited coming
    from a different node than the original caller.
    """
    paths = 0
    state = MAZE[row][col]
    if state == '$':
        # Reached end, add one valid path from this node
        paths += 1
    if state == '+' or state == '@':
        # Reached a bifurcation, must do recursion. Once computed, store in
        # MAZE so it needn't be recomputed later.
        if row < len(MAZE) - 1:
            paths += count_paths(row+1, col)
        if col < len(MAZE[0]) - 1:
            paths += count_paths(row, col+1)
            MAZE[row][col] = paths
    if isinstance(state, int):
        # The paths from this point have been previously calculated (above),
        # so we just add them
        paths += state
    # if state is 'X': don't do anything
    return paths

def main():
    """ Read codeabbey usual test cases
    """
    data = open("DATA.lst", "r")
    lines = int(data.readline().split()[0])
    for _dummy in range(lines):
        MAZE.append((data.readline().split()))
    data.close()
    print(count_paths(0, 0))

main()
