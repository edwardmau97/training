/*
$ eslint jarboleda.js
$
*/


function solveFor(matrix, currentHeight, currentWidth) {
  if (currentHeight < matrix.length && currentWidth < matrix[0].length) {
    if (matrix[currentHeight][currentWidth] === '@' ||
      matrix[currentHeight][currentWidth] === '+') {
      return solveFor(matrix, currentHeight + 1, currentWidth) +
        solveFor(matrix, currentHeight, currentWidth + 1);
    } else if (matrix[currentHeight][currentWidth] === '$') {
      return 1;
    }
  }
  return 0;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const firstInputLine = contents.split('\n')[0].split(' ');

  const matrix = contents.split('\n').slice(1, firstInputLine[0] + 1)
    .map((line) =>
      (line.split(' ').slice(0, firstInputLine[1])));
  const answer = solveFor(matrix, 0, 0);
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
3547333
*/
