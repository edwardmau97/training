-- $ ghc -o smendoz3 smendoz3.hs-
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad
import Data.List

main = do
  input <- getLine
  print (unwords (repeated (words input)))

repeated :: Ord a => [a] -> [a]
repeated = repeatedBy (>1)

repeatedBy :: Ord a => (Int -> Bool) -> [a] -> [a]
repeatedBy p = map head . filterByLength p

filterByLength :: Ord a => (Int -> Bool) -> [a] -> [[a]]
filterByLength p = filter (p . length) . sg

sg :: Ord a => [a] -> [[a]]
sg = group . sort

-- $ ./smendoz3
--   bat bef bep bof bok bup byq dak dix dox duh duk get gex gix gos gup
--   jeq jik jix jop juf jus jux jyk lap let lih lip los lyc lyp lys lyx
--   mah mip moc moq myk nac nat nec niq nis nix noc nup nux nyt raq rek
--   rih rix rox vek vik vot vux vyc zep zik zoc zof zut zux zys
