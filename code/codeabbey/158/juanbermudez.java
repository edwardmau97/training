package RETO158;

import java.util.ArrayList;

public class HammingCodes {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    String[] data0= {"110100000","00110001010010","1011101011","10111110110111110110011100",
                     "1010110000111111001","101111100100101010","0100110111011001000",
                     "10010011001001000","100001010111","111000111110111000",
                     "0101111011011010100","0000111101","10111010","0010"};
    String[] data1= {"0001010000001010000","111010100110011110111110100011",
                     "1100001011010000010001101110","11011010011001101110010101011",
                     "110011101001010000011000","100101011101111110101001100010",
                     "011100101001011111101","10100011010001111101011",
                     "0110110011000001111110000","1100010100001111010101011111100",
                     "100001101001011110100100","11001101010101010101010",
                     "0011111100100","1001111101010101111110101"};
 
    for (String encode : data0) {
      ArrayList<Integer> calculeCode=new ArrayList<Integer>();
      int cntPow=1,cntCode=0;
      while (cntCode<encode.length()) {
        int number1=cntPow,number2=cntPow-1;
        int powCheck=number1&number2;
        if (powCheck==0) {
          calculeCode.add(null);
        }else {
          calculeCode.add(Character.getNumericValue(encode.charAt(cntCode)));    
          cntCode++;
        }
        cntPow++;
      }
      HammingValuesEncode hammingValuesEncode = new HammingValuesEncode(encode,(calculeCode.size()-encode.length()));
      for (Integer dataCode : hammingValuesEncode.getArrayHammingData()) {
        System.out.print(dataCode);
      }
      System.out.print(" ");
    }
    
    //System.out.println("++++++++++++++++");
    
    for (String decode : data1) {
      int cntCode=0;
      for (int i = 1; i < decode.length()+1; i++) {
        int number1=i,number2=i-1;
        int powCheck=number1&number2;
        if (powCheck!=0) {
          cntCode++;
        }
      }
      //System.out.println(decode.length()+"-"+cntCode);
      HammingValuesDecode hammingValuesDecode = new HammingValuesDecode(decode,decode.length()-cntCode);
      for (Integer dataDecode : hammingValuesDecode.getArrayData()) {
        System.out.print(dataDecode);
      }
      System.out.print(" ");
    }
    
 
  }
}

class HammingValuesEncode{
  public HammingValuesEncode(String data,int code) {
    // TODO Auto-generated constructor stub
    int cnt0=0;
    this.code=code;
    arrayData=new Integer[data.length()];
    arrayHammingData=new Integer[data.length()+code];
    arrayParity=new Integer[code];
    for (int i = 0; i < arrayData.length; i++) {
      arrayData[i]=Character.getNumericValue(data.charAt(i));
      //System.out.println(arrayData[i]);
    }
    for (int i = 1; i < arrayHammingData.length+1; i++) {
      int number1=i,number2=i-1;
      int powCheck=number1&number2;
      if (powCheck==0) {
        arrayHammingData[i-1]=null;
      }else {
        arrayHammingData[i-1]=arrayData[cnt0];
        cnt0++;
      }
    }
    /*for (Integer integer : arrayHammingData) {
      System.out.print(" "+integer);
    }
    System.out.println();
    System.out.println("-----------------------------------");*/
    arrayParity=CalculateParity(arrayHammingData);
   
    /*System.out.println();
    for (Integer integer : arrayParity) {
      System.out.print(" "+integer); 
    }*/
  }
  public Integer[] getArrayHammingData() {
    int cnt=0;
    for (int i = 0; i < arrayHammingData.length; i++) {
      if (arrayHammingData[i]==null) {
        arrayHammingData[i]=arrayParity[cnt];
        cnt++;
      }
    }
    return arrayHammingData;
  }
  protected Integer[] CalculateParity(Integer[] arrayHammingData) {
    int cntCode=0;
    for (Integer hamming : arrayHammingData) {
      if (hamming==null) {
        cntCode++;
      }
    }
    this.cntCode=cntCode;
    Integer[] arrayParity=new Integer[cntCode];
    int zeroCounter=0;
    int oneCounter=0;
    int pow2=1;
    for (int i = 0; i < cntCode; i++) {
      for (int j = 1; j < arrayHammingData.length+1; j++) {
        if (arrayHammingData[j-1]!=null) {
          
          if ((j&pow2)==pow2) {
            //System.out.print(" "+arrayHammingData[j-1]);
            if (arrayHammingData[j-1]==1) {
              oneCounter++;
            }else if (arrayHammingData[j-1]==0) {
              zeroCounter++;
            }
          }else {
            //System.out.print(" *");
          }
        }else {
          //System.out.print(" "+arrayHammingData[j-1]);
        }
      }
      if (oneCounter!=0) {
        if (oneCounter%2==0) {
          arrayParity[i]=0;
        }else {
          arrayParity[i]=1;
        }
      }else {
        arrayParity[i]=0;
      } 
      pow2=2*pow2;
      oneCounter=0;
      zeroCounter=0;
      //System.out.println();
    }    
    return arrayParity;
  }

  private int code,cntCode;
  private Integer[] arrayData,arrayHammingData,arrayParity;
}

class HammingValuesDecode extends HammingValuesEncode{
  public HammingValuesDecode(String data, int code) {
    super(data, code);
    int cnt0=0,cnt1=0;
    int cntCode=0;
    this.code=code;
    
    arrayHammingData=new Integer[data.length()];
    
    for (int i = 0; i < arrayHammingData.length; i++) {
      arrayHammingData[i]=Character.getNumericValue(data.charAt(i));
      int number1=i+1,number2=i;
      int powCheck=number1&number2;
      if (powCheck==0) {
        cntCode++;
      }
    }
    this.cntCode=cntCode;
    arrayParity=new Integer[cntCode];
    arrayParityStored=new Integer[cntCode];
    arrayData=new Integer[data.length()-cntCode];
    
    for (int i = 1; i < arrayHammingData.length+1; i++) {
      int number1=i,number2=i-1;
      int powCheck=number1&number2;
      if (powCheck==0) {
        arrayParityStored[cnt0]=arrayHammingData[i-1];
        arrayHammingData[i-1]=null;
        cnt0++;
      }else {
        arrayData[cnt1]=arrayHammingData[i-1];
        cnt1++;
        //System.out.println("array data="+arrayHammingData[i-1]);
      }
    }
     
    arrayParity=CalculateParity(arrayHammingData);  
  }

  public Integer[] getArrayData() {
    syndrome = getSyndrome(arrayParity, arrayParityStored);
    if (syndrome==0) {
      return arrayData;
    }else {
      if (arrayHammingData[syndrome-1]!=null) {
        if (arrayHammingData[syndrome-1]==0) {
          arrayHammingData[syndrome-1]=1;
        }else if (arrayHammingData[syndrome-1]==1) {
          arrayHammingData[syndrome-1]=0;
        }
      }      
      int cnt=0;
      for (int i = 0; i < arrayHammingData.length; i++) {
        if (arrayHammingData[i]!=null) {
          arrayData[cnt]=arrayHammingData[i];
          cnt++;
        }
      }
      return arrayData;
    }
    
  }

  private int getSyndrome(Integer[] arrayParity,Integer[] arrayParityStored) {
    
    int syndrome=0;
    for (int i = 0; i < arrayParityStored.length; i++) {
      if (arrayParity[i]!=arrayParityStored[i]) {
        syndrome=(int)Math.pow(2,i)+syndrome;
      }
    }
    return syndrome;
  }
  
  private int code,cntCode,syndrome;
  private Integer[] arrayData,arrayHammingData,arrayParity,arrayParityStored;
}
