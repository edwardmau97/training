// $ eslint --global require jdanilo7.js #linting
// $ node --harmony jdanilo7.js #compilation

// unnecesary or conflicting rules
/* eslint-disable no-console */
/* eslint-disable no-magic-numbers */
/* eslint-disable no-sync */
/* eslint-disable array-element-newline */
/* eslint-disable filenames/match-regex */

const kvArray = [
  [ 1, 'A' ], [ 2, 'B' ], [ 3, 'C' ], [ 4, 'D' ], [ 5, 'E' ],
  [ 6, 'F' ], [ 7, 'G' ], [ 8, 'H' ], [ 9, 'I' ], [ 10, 'J' ], [ 11, 'K' ],
  [ 12, 'L' ],
];
const mapp = new Map(kvArray);

function swap(kindex, lindex, list) {
  const head = list.slice(0, kindex);
  const body = list.slice(kindex + 1, lindex);
  const headl = head.concat(list[lindex]);
  const bodyk = body.concat(list[kindex]);
  if (lindex < list.length - 1) {
    const tail = list.slice(lindex + 1);
    return headl.concat(bodyk).concat(tail);
  }
  return headl.concat(bodyk);
}

function reverse(kindex, list) {
  const head = list.slice(0, kindex + 1);
  const reversed = list.slice(kindex + 1).reverse();
  return head.concat(reversed);
}

function findk(i, list, length, kindex) {
  if (i === length) {
    return kindex;
  }
  if (list[i] < list[i + 1]) {
    return findk(i + 1, list, length, i);
  }
  return findk(i + 1, list, length, kindex);
}

function findl(i, list, length, lindex, kval) {
  if (i === length) {
    return lindex;
  }
  if (list[i] > kval) {
    return findl(i + 1, list, length, i, kval);
  }
  return findl(i + 1, list, length, lindex, kval);
}

function permute(lst) {
  const kindex = findk(0, lst, lst.length - 1, -1);
  const lindex = findl(kindex + 1, lst, lst.length, kindex + 1, lst[kindex]);
  if (kindex === -1) {
    return lst;
  }
  const swapped = swap(kindex, lindex, lst);
  return reverse(kindex, swapped);
}

function findInitial(list, remAttempts) {
  if (remAttempts === 0) {
    return list;
  }
  const permuted = permute(list);
  return findInitial(permuted, remAttempts - 1);
}

function buildString(i, length, list, map, res) {
  if (i === length) {
    return res;
  }
  const val = map.get(list[i]);
  return buildString(i + 1, length, list, map, res.concat(val));
}

function evalInput(list, length, i, res) {
  if (i === length) {
    return res;
  }
  const orderedList = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ];
  const prevAttempts = parseInt(list[i], 10);
  const initial = findInitial(orderedList, prevAttempts);
  const result = buildString(0, orderedList.length, initial, mapp, '');
  return evalInput(list, length, i + 1, res.concat(result).concat(' '));
}

const filesys = require('fs');
function main() {
  const contents = filesys.readFileSync('DATAo.lst', 'utf8').split('\r\n');
  const input = contents.slice(1, contents.length - 1);
  const output = evalInput(input, input.length, 0, '');
  console.log(output);
}

main();

// kLFCEHBIJGAD LGDIFKEBJCHA JIFDHLCAKBGE DFABGHLJKIEC AFDBGJELKHCI
// eHAKFBDCIJGL FALIEJDBCGHK KCEGJLHDAIBF FKIDCLGJAHEB EKBCLIJAHDFG
// jGKHILCDABEF ICKJGBHDEALF GJCKABEHDFIL JAEKBDHICFGL FJCLGKIAEBDH
// gFBDCAKLEIHJ GAECHDKBFLIJ CAHIKBJDGELF AEFIDJKBGCHL CDHEBLIKAJFG
// lDJEKHAFGBIC DFJLEHICKBAG AJIFDHCBGELK
