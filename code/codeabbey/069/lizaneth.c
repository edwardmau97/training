/*
$ cppcheck --enable=all --inconclusive --language=c lizaneth.c #linting
$ gcc lizaneth.c -o exe #compilation
$ c lizaneth.c
$
*/

int main(){
    int i = 0;
    int k = 2;
    int n = 0;
    int M = 0;
    int f1 = 0;
    int f2 = 1;
    int f3 = 1;
    FILE *file = 0;
    if ((file = fopen("DATA.lst","r")) == NULL){
       printf("Error");
       exit(0);
    }
    (void)fscanf(file,"%d", &n);
    for (i = 1; i < n+1; i++){
        (void)fscanf(file,"%d", &M);
        while (f3 != 0){
           f3 = (f2 + f1) % M;
           f1 = f2;
           f2 = f3;
           k++;
        }
        printf("%d ", k-1);
        k = 2;
        f1 = 0;
        f2 = 1;
        f3 = 1;
    }
    (void)fclose(file);
    return 0;
}

/*
$ ./exe
102 396 7728 1914 990 840 750 900 2700 8700 1592 72 552 807 1000 2688 270 2100
 3738 300
*/
