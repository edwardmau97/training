# gdlint john2104.gd
# Success: no problems found

extends Node

func _ready():
    var input = return_data()
    var array = input.split(" ")
    var one = int(array[0])
    var two = int(array[1])
    print(str(one+two))

func return_data():
    var file = File.new()
    file.open("res://codeabbey_001/DATA.lst", File.READ)
    return file.get_as_text()

# Create a scene in godot and attach this script
# Run the scene
#
# ** Debug Process Started **
# OpenGL ES 3.0 Renderer: Mesa DRI Intel(R) HD Graphics 520 (Skylake GT2)
# 30584
