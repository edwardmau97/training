/*
$ linter language-prolog
$ prolog Cha0smagick.pl
$
*/
main:-
    open(’OTHERS.lst’,read,Int),
    read(Int,Num1),
    read(Int,Num2),
    close(Int),
sum(Num1,Num2):-
    C is Num1+Num2,
    write(C).

/*
$ ./Cha0smagick
4419 14353
*/
