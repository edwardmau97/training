# $ lintr::lint('mrivera3.r')


absum <- function() {
  "adding a+b"
  data <- read_table2("DATA.lst", col_names = FALSE)
  results <- sum(data)
  return(results)
}
absum()

# $ Rscript mrivera3.r
# 18772
