/*
$ rustfmt adrianfcn.rs
$ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
$ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let mut file = File::open("DATA.lst")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    for line in contents.lines() {
        let mut params = vec![];
        for c in line.split_whitespace() {
            params.push(c);
        }
        let p: f64 = params[0].parse().ok().expect("only numbers");
        let t: f64 = params[1].parse().ok().expect("only numbers");
        let r: f64 = (t / 100.0) / 12.0;
        let l: i32 = params[2].parse().ok().expect("only numbers");
        let m = (p * r * (1.0 + r).powi(l)) / ((1.0 + r).powi(l) - 1.0);
        println!("{}", m.round());
    }
    Ok(())
}
/*
  ./adrianfcn
  12764
*/
