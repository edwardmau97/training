let data = ";>;>;<<[->>[->+>+<<]>[-<<+>>]<<[->+<]>:>>[-<<<+>>>]<<<<]";;
print_string "ingresa los numeros separados por un espacio \n";;
let dataNum = (read_line ());;
let numList = List.map int_of_string(Str.split (Str.regexp " ") dataNum);;
let leng = String.length data;;
let cel = Array.make 5 0;;
let c = ref 0;;
let p = ref 0;;
let k = ref 0;;
let j = ref 0;;
let t = ref 0;;
let flag = ref true;;
let rec decode x =
    match x with
    | '+' -> cel.(!p)<-cel.(!p) + 1;flag:=true;
    | '-' -> cel.(!p)<-cel.(!p) - 1;flag:=true;
    | '>' -> p := !p + 1;flag:=true;
    | '<' -> p := !p - 1;flag:=true;
    | ':' -> Printf.printf "%i " cel.(!p);flag:=true;
    | ';' -> (
                cel.(!p)<-(List.nth numList !k);
                k:=!k+1;                
             )
    | '[' -> (        
                flag:=true;
                if !c <> 7 then(
                    j:=!c+1;
                    t:=!c+1;            
                    while data.[!j]<>']' do            
                        j:=!j+1
                    done;
                    if cel.(!p)=0 then (
                        c:=!j+1;
                        flag:=false;                    
                    )else(                    
                        flag:=false;
                        c:=!c+1;                        
                    );
                );
             )    
    | ']' -> (                
                if !c=55 then(
                    if(cel.(!p)=0)then((*necesario para que se incremente solo el valor de c y salga del bucle*)
                    )else(
                        flag:=false;
                        c:=7;
                    )
                )else(
                    if cel.(!p)=0 then (
                     )else(
                         flag:=false;
                         c:=!t;                        
                     )
                )     
             )    
    | _ -> failwith "se ingreso un caracter no valido\n"
;;
while !c<leng do    
    decode data.[!c];
    if(!flag)then(        
        c := !c + 1;
    )    
done;;
print_string "\n";;
