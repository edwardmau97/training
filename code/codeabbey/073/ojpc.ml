let rec movey step =
        if   step = "A"
            then  0.0
            else movey2 step
and movey2 step =
        let pi = 4.0 *. atan 1.0 in
            if   step = "B"
            then cos(30.0*.(pi/.180.0))
            else movey3 step
and movey3 step =
        let pi = 4.0 *. atan 1.0 in
            if   step = "C"
            then cos(30.0*.(pi/.180.0))
            else movey4 step
and movey4 step =
        if   step = "D"
            then 0.0
            else movey5 step
and movey5 step =
        let pi = 4.0 *. atan 1.0 in
        if   step = "E"
            then -.cos(30.0*.(pi/.180.0))
            else -.cos(30.0*.(pi/.180.0));;

let rec movex step =
        if   step = "A"
            then  1.0
            else movex2 step
and movex2 step =
        let pi = 4.0 *. atan 1.0 in
            if   step = "B"
            then sin(30.0*.(pi/.180.0))
            else movex3 step
and movex3 step =
        let pi = 4.0 *. atan 1.0 in
            if   step = "C"
            then -.sin(30.0*.(pi/.180.0))
            else movex4 step
and movex4 step =
        if   step = "D"
            then -1.0
            else movex5 step
and movex5 step =
        let pi = 4.0 *. atan 1.0 in
        if   step = "E"
            then -.sin(30.0*.(pi/.180.0))
            else sin(30.0*.(pi/.180.0)) ;;

let inp=["BBBDCDFAABCCCCBDCFBBCFA";"FBDCCCDADAFEBCCD";"CCCFECCFCB";"CBDBAFEDBBCCADDADDEDFFBACF";"FACE";"BAEEAB";"CFAABAEFBCFABCAD";"CACABAFEACCBBECFE";"EAABEED";"AFDDDADDFBDFBC";"BABEBDCDAEFCEBCDBEDCCADEFEBDF";"FBDBFACDDBCBCCAAA";"AAA";"AEFABEBCCDEABCBBDE";"EDECEACBEADDBFEBFEF";"ABDAEDABBCADCAFDBCCE";"ABAADBABEFDFBCFEECEFAACBDDFCF";"CAFBACEEFFFBAAAADEBAAECB";"CABAECCCBDDEEBCBFBDDFBFDDCBCFB";"EBAFDAABBCBFAFDB";"FBCFCBFBDA";"CFEC";"AEBBCFCADDED";"BBBAEAAEDBBFFFF"];;
let split= Str.split (Str.regexp "");;

let out=
    for i=0 to List.length inp -1 do
        let word= List.nth inp i in
        let wsplit= split word in
        let x=ref 0.0 in
        let y=ref 0.0 in
        let res=ref 0.0 in
        for j=0 to List.length wsplit -1 do
          let step= List.nth wsplit j in
             x :=    !x +. (movex step) ;
             y := !y +. (movey step) ;
             if j= List.length wsplit -1 then
                    begin
                        res:= sqrt(!x**2.0 +. !y**2.0);
                        print_float !res;
                        print_string " ";
                    end
            done;
        done;
