/*
$ splint adrianfcn.c
Splint 3.1.2 --- 01 Dec 2016

Finished checking --- no warnings
$ cppcheck adrianfcn.c
Checking adrianfcn.c ...
$ gcc adrianfcn.c -o output -lm -O3 -Wall -Wextra -std=c89 -pedantic-errors
-Wmissing-prototypes -Wstrict-prototypes -Wold-style-definition
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MAXBUFF 1000

struct base32 {
  char c;
  char binary[6];
};

static void convert_ascii_bin(int dec,char binary[]);
static void convert_base32_bin(char base32,char *binary);
static void union_letters_bin(char *phrase,char *binphrase);
static void union_letters(char *phrase,char *binphrase);
static char convert_bin_base32(char binary[]);
static void del_enter(char *,char *);
static char convert_bin_ascii(char binary[]);
static void to_complete(char *chain);
static int length(char *p);
static int is_letter(char c);
static void encode(char *p);
static void decode(char *p);


static struct base32 listbase32[] = { {'A',"00000"},{'B',"00001"},
                                      {'C',"00010"},{'D',"00011"},
                                      {'E',"00100"},{'F',"00101"},
                                      {'G',"00110"},{'H',"00111"},
                                      {'I',"01000"},{'J',"01001"},
                                      {'K',"01010"},{'L',"01011"},
                                      {'M',"01100"},{'N',"01101"},
                                      {'O',"01110"},{'P',"01111"},
                                      {'Q',"10000"},{'R',"10001"},
                                      {'S',"10010"},{'T',"10011"},
                                      {'U',"10100"},{'V',"10101"},
                                      {'W',"10110"},{'X',"10111"},
                                      {'Y',"11000"},{'Z',"11001"},
                                      {'2',"11010"},{'3',"11011"},
                                      {'4',"11100"},{'5',"11101"},
                                      {'6',"11110"},{'7',"11111"}};

int main(void) {
  int i = 0;
  char s[MAXBUFF] = "";
  char s2[MAXBUFF] = "";
  FILE *fp;
  fp = fopen("DATA.lst","r");
  /*@-nullpass@*/
  (void)fgets(s2,MAXBUFF,fp);
  del_enter(s,s2);
  for (i = 0;i < atoi(s);i++) {
    (void)fgets(s2,MAXBUFF,fp);
    if ((i + 1)%2 != 0) {
      encode(s2);
    } else {
      decode(s2);
    }
    printf(" ");
  }
  (void)fclose(fp);
  return 0;
}

void decode(char *phrase) {
  unsigned int i = 0,j = 0;
  char binaryphrase[MAXBUFF] = "";
  char box[9] = "";
  union_letters(phrase,binaryphrase);
  for (i = 0;i < (unsigned)strlen(binaryphrase);i += 8) {
    for (j = 0;j < 8;j++) {
      box[j] = binaryphrase[i + j];
    }
    box[8] = '\0';
    if (is_letter(convert_bin_ascii(box)) != 0
      || convert_bin_ascii(box) == ' ') {
      printf("%c",convert_bin_ascii(box));
    }
  }
}

void encode(char *phrase) {
  unsigned int i = 0,j = 0;
  char binaryphrase[MAXBUFF] = "";
  char box[6] = "";
  to_complete(phrase);
  union_letters_bin(phrase,binaryphrase);
  for (i = 0;i < (unsigned)strlen(binaryphrase);i += 5) {
    for (j = 0;j < 5;j++) {
      box[j] = binaryphrase[j + i];
    }
    box[5] = '\0';
    printf("%c",convert_bin_base32(box));
  }
}

void to_complete(char *pchain) {
  int count = 0,i = 0;
  char box[2] = "";
  if (length(pchain)%5 == 0) {
    do {
      count++;
    } while((length(pchain) + count)%5 != 0);
  } else {
    while((length(pchain) + count)%5 != 0) {
      count++;
    }
  }
  box[0] =(char) (48+ count);
  box[1] = '\0';
  for (i = 0;i < count;i++) {
    strcat(pchain,box);
  }
}

void union_letters_bin(char *phrase,char *binphrase) {
  unsigned int i = 0;
  char binletter[9] = "";
  for (i = 0;i < (unsigned)strlen(phrase);i++) {
    if (phrase[i] != '\n') {
      convert_ascii_bin((int)phrase[i],binletter);
      strcat(binphrase,binletter);
    }
  }
}

void union_letters(char *phrase,char *binphrase) {
  unsigned int i = 0;
  char binletter[6] = "";
  for (i = 0;i < (unsigned)strlen(phrase);i++) {
    if (phrase[i] != '\n') {
      convert_base32_bin(phrase[i],binletter);
      strcat(binphrase,binletter);
    }
  }
}

void convert_ascii_bin(int ascii,char binary[]) {
  int i = 0;
  char boxbinary[9] = "";
  for (i = 0;i < 8;i++) {
    if (ascii%2 == 0) {
      boxbinary[i] = '0';
    } else {
      boxbinary[i] = '1';
    }
    ascii = ascii/2;
  }
  boxbinary[8] = '\0';
  for (i = 0;i < 8;i++) {
    binary[i] = boxbinary[7-i];
  }
  binary[8] = '\0';
}

char convert_bin_ascii(char binary[]) {
  int n = 0,i = 0,j = 0;
  for (i = 7,j = 0;i > -1;j++,i--) {
    if (binary[i] != '0') {
      n += pow(2,(double)j);
    }
  }
  return (char)n;
}

char convert_bin_base32(char binary[]) {
  int i = 0;
  for (i = 0;i < 32;i++) {
    if (strcmp(listbase32[i].binary,binary) == 0) {
      return listbase32[i].c;
    }
  }
  return (char)0;
}

static void convert_base32_bin(char base32,char *binary) {
  int i = 0;
  for (i = 0;i < 32;i++) {
    if (base32 == listbase32[i].c) {
      strcpy(binary,listbase32[i].binary);
    }
  }
}

int length(char *p) {
  int len = 0,i = 0;
  while (p[i] != '\0' && p[i] != '\n') {
    len++;
    i++;
  }
  return len;
}

void del_enter(char *c,char *c2) {
  int i = 0;
  while (c2[i] != '\n') {
    c[i] = c2[i];
    i++;
  }
  c2[i] = '\0';
}

int is_letter(char c) {
  if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
    return 1;
  }
  return 0;
}

/* $ ./output
OBQXE4TPOQ2DINBU induced rejuvenation MVTGMZLSOZSXGY3FNZ2DGMZT
delectation cuddlier pasty
OVXGSY3ZMNWGK4ZAMZXXE3LVNRQWKIDXOJSXG5DMNFXGOIDMNFTWQ5DIMVQWIZLEGU2TKNJV
perfect reappearances calyxes burred suavity
N5VWC6LTEB2W44DMOVTSAZLGMZSWG5DVMF2GKIDBNZ2GK3TOMFSTGMZT
heirlooms putter deformed hydrae
NFWW22LHOJQXIZLEEBZXOZLFORWHSMRS
mutates NVUXGY3FM5SW4YLUNFXW4MRS
fulfilled competitive normalized enumerated
NZSXO43XN5ZHI2DJMVZXIIDTNFTW42LGNFRWC5DJN5XHGIDGMFRWS3DJORQXI2LOM42DINBU
limericks impeached MZ2XE3TBMNSXGIDCMFWGYYLEOM2DINBU
estimating fluky unmindful wealth ripens
MNZGKYLUN5ZCAY3PNVYHK5DFOJUXUYLUNFXW4IDVNZWWK4TDNFTHK3DMPEQHAZLSNFRWC4T
ENF2W2MRS
wastrels MNXXK3TDNFWHO33NMVXDGMZT
woks mismatches punchier
ON2GK4TOMVSCAY3SN5ZXGYTBOJZGKZBAMNUHKY3LGU2TKNJV
magpies freehand chloroform utilizing
NRUWWZLMNFUG633EOMQGCZDNNF2HIZLEEBXGS4DQMVZGKZBAONVWS4TNNFZWQ2LOM42DINBU
maize selvedging MNWGKYLUOM2DINBU dramatic boomerang oversensitive
existentially bathes */
