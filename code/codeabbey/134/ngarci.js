/* eslint brace-style: ["error", "stroustrup"]*/
/* eslint no-console: ["error",{ allow: [log] }]*/
/*
$ eslint ngarci
*/

const files = require('fs');
const path = require('path');
const results = [];
const steps = 100;
const moveRight = 1;
const moveDown = 1;
const moveLeft = -1;
const moveUp = -1;

files.readFile(path.join(__dirname, 'DATA.lst'), 'utf-8', (fail, inputData) => {
  if (fail) {
    throw fail;
  }
  const [ Width, Height, Length ] = inputData
    .split(' ')
    .map((x) => Number(x));

  let count = 0;
  let markHorizontal = 0;
  let markVertical = 0;
  let flagValHorizontal = 1;
  let flagValVertical = 1;
  while (count <= steps) {
    const coordinates = [ markHorizontal, markVertical ];
    const otherSide = [ markHorizontal + Length - 1, markVertical ];
    results.push(coordinates);
    if (otherSide[0] === Width - 1) {
      flagValHorizontal = moveLeft;
    }
    if (coordinates[0] === 0) {
      flagValHorizontal = moveRight;
    }
    if (otherSide[1] === Height - 1) {
      flagValVertical = moveUp;
    }
    if (coordinates[1] === 0) {
      flagValVertical = moveDown;
    }
    markHorizontal += flagValHorizontal;
    markVertical += flagValVertical;
    count++;
  }
  console.log(results.flat().map((x) => x.toString()).join(' '));
});

/*
$ js ngarci
output:
0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13
13 14 14 15 15 16 16 17 17 18 16 19 15 20 14 21 13 22 12 23
11 24 10 25 9 26 8 27 7 28 6 29 5 30 4 31 3 32 2 33 1 34 0 35
1 34 2 33 3 32 4 31 5 30 6 29 7 28 8 27 9 26 10 25 11 24 12 23
13 22 14 21 15 20 16 19 17 18 16 17 15 16 14 15 13 14 12 13 11
12 10 11 9 10 8 9 7 8 6 7 5 6 4 5 3 4 2 3 1 2 0 1 1 0 2 1 3 2
4 3 5 4 6 5 7 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13 15 14 16
15 17 16 16 17 15 18 14 19 13 20 12 21 11 22 10 23 9 24 8 25
7 26 6 27 5 28 4 29 3 30 2
*/
