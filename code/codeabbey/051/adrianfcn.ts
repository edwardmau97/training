/*
  $ tslint adrianfcn.ts
  $ tsc adrianfcn.ts
*/
import * as fs from "fs";

class Dice {
  public dxy: string;
  public values: number[];
  constructor(dxy: string, values: number[]) {
    this.dxy = dxy;
    this.values = values;
  }
}

function sum(values) {
  let num = 0;
  for (const i of values) {
    num += i;
  }
  return num;
}

function gen_table() {
  const probTable: Dice[] = [];
  for (let i = 1; i < 6; i++) {
    for (let j = 2; j < 13; j += 2) {
      const key: string = i + "d" + j;
      const values: number[] = [i * 100, i * 100 * j, i, i * j];
      probTable.push(new Dice(key, values));
    }
  }
  return probTable;
}

function min(values) {
  let num = 1000;
  for (const i of values) {
    num = Math.min(num, i);
  }
  return num;
}

function max(values) {
  let num = 0;
  for (const i of values) {
    num = Math.max(num, i);
  }
  return num;
}

function solution(values, probTable) {
  const sumation  = sum(values);
  const maximum = max(values);
  const minimum = min(values);
  let initialProb = 0.0;
  let possibleSolution = "";
  for (const items of probTable) {
    if (minimum >= items.values[2] && maximum <= items.values[3]
       && sumation >= items.values[0]) {
      const finalProb = (1 / items.values[1]) * items.values[0];
      if (finalProb > initialProb) {
        possibleSolution = items.dxy;
        initialProb = finalProb;
      }
    }
  }
  return possibleSolution;
}

function main() {
  const data = fs.readFileSync("DATA.lst", "utf8");
  const arr = data.split("\n");
  let sol = "";
  const probTable: Dice[] = gen_table();
  arr.pop();
  for (const i of arr) {
    const names: string[] = [];
    const values: number[] = [];
    const v = i.split(" ");
    for (let j = 0; j < v.length - 1; j++) {
      values.push(parseInt(v[j], 10));
    }
    sol += solution(values, probTable) + " ";
  }
  // tslint:disable-next-line:no-console
  console.log(sol);
}

main();
/*
  $ tsc adrianfcn.ts
  2d8 5d6 2d6
*/
