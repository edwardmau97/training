{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = map read $ words line :: [Float]
        let a = head vector_ints
        let b = vector_ints!!1
        let c = vector_ints!!2
        let result = b ^ 2 - 4 * a * c
        let n1 = round((-b + sqrt result) / (2*a))
        let n2 = round((-b - sqrt result) / (2*a))
        let n3 = round(-b / (2*a))
        let n4 = round(sqrt(-result) / (2*a))
        if result >= 0
          then print(show n1 ++ " " ++ show n2 ++ ";")
          else print(show n3++"+"++show n4++"i "++show n3++"-"++show n4++"i;")
        processfile ifile


main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 6 3; 6 -6; 3 -5; 10 7; 4+9i 4-9i; -3+8i -3-8i; -9+7i -9-7i;
-7+1i -7-1i; 3 2; 10 -2; -5+7i -5-7i; -1+1i -1-1i
-}
