/*
$ cargo clippy
Checking neds v0.1.0 (/home/ndsanchez/Code/rust/neds)
Finished dev [unoptimized + debuginfo] target(s) in 0.34s
$ cargo run
Compiling neds v0.1.0 (/home/ndsanchez/Code/rust/neds)
Finished dev [unoptimized + debuginfo] target(s) in 1.37s
Running `target/debug/neds`
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let filepath = "src/DATA.lst";
  let file = File::open(filepath).unwrap();
  let reader = BufReader::new(file);
  // Read the file
  for (_index, line) in reader.lines().enumerate() {
    let line = line.unwrap();
    let data: Vec<&str> = line.split(' ').collect();
    if _index != 0 {
      let vala = data[0].parse::<f32>().unwrap();
      let valb = data[1].parse::<f32>().unwrap();
      let valc = data[2].parse::<f32>().unwrap();
      find_roots(vala, valb, valc);
    }
  }
}

fn find_roots(vala: f32, valb: f32, valc: f32) {
  let alfa = valb.powf(2.0) - 4.0 * vala * valc;
  if alfa >= 0.0 {
    let root_one: f32 = (- valb + alfa.sqrt()) / (2.0 * vala);
    let root_two: f32 = (- valb - alfa.sqrt()) / (2.0 * vala);
    print!("{} {}; ", root_one, root_two);
  }
  else {
    let real = valb * (-1.0) / (2.0 * vala);
    let imag = alfa.abs().sqrt() / (2.0 * vala);
    print!("{}+{}i {}-{}i; ", real, imag , real, imag);
  }
}

/*
$ cargo run
6 3; 6 -6; 3 -5; 10 7; 4+9i 4-9i; -3+8i -3-8i; -9+7i -9-7i;
-7+1i -7-1i; 3 2; 10 -2; -5+7i -5-7i; -1+1i -1-1i
*/
