#!/usr/bin/env python3
"""
Problem #16 Average of an array
"""
class AverageOfArray:

    result = []

    def __init__(self):

        while True:

            line = input()
            if line:

                if len(line) < 3:
                    pass
                else:
                    data = line.split()
                    self.average(*data)
            else:
                break

        text = ' '.join(self.result)
        print(text)

    def average(self, *data):
        total_average = 0
        for i, item in enumerate(data):
            value = int(item)
            if value > 0:
                total_average += value
        total_average = total_average/(len(data)-1)
        self.result.append(str(self.rounding(total_average)))

    def rounding(self, average):
        if (average % 2) > 0.5:
            return int(average + 0.5)
        else:
            return int(average)

AverageOfArray()
