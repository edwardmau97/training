# $ lintr::lint('mrivera3.r')

sumarray <- function() {
  data <- read_csv("DATA.lst", col_names = FALSE, skip = 1)
  results <- c()
  for (i in seq_len(nrow(data))) {
    prov <- as.character(data$X1[i])
    int <- as.integer(str_split(prov, " ")[[1]])
    results <- c(results, round(sum(int) / (length(int) - 1)))
  }
  return(results)
}
sumarray()

# $ Rscript mrivera3.r
# 6375  231  421 3489  145  407  209 8453  145  974  451 3330
