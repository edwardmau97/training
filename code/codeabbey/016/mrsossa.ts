/*
 $ tslint mrsossa.ts
*/

import * as fs from "fs";
import * as readline from "readline";
const rl = readline.createInterface({
  input: fs.createReadStream("DATA.lst"),
});
let l = 0;
rl.on("line", function(line) {
  let sum = 0;
  if (l === 0) {
    l = 1;
  }
  else {
    const token = line.split(" ");
    for (const toke of token) {
      sum += +toke;
    }
    console.log(Math.floor(sum / (token.length - 1)));
  }
});

/*
 $ tsc mrsossa.ts
 $ node mrsossa.js
 6375 230 420 3488 145 407 208 8452 144 974 450 3329
*/
