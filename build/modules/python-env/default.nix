pkgs:

pkgs.python37.withPackages (ps: with ps; [
  pip
  setuptools
  wheel
])
