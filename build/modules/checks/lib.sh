#! /usr/bin/env bash

function check_directory_depth {
  local bad_folders

  find code -mindepth 3 -type d >&"${TEMP_FD}"
  mapfile -t bad_folders <&"${TEMP_FD}"

  if test "${#bad_folders[@]}" -eq 0
  then
    echo '[OK] Directory depth is below threshold'
    return 0
  else
    echo "${bad_folders[@]}"
    echo '[ERROR] Directory depth is limited to 3, example: code/a/b'
    return 1
  fi
}

function check_for_duplicates_in_others_lst {
  if ./build/modules/checks/find_duplicated_urls_in_others_lst.py
  then
    echo '[OK] OTHERS.lst seem to contain unique urls'
    return 0
  else
    echo '[ERROR] OTHERS.lst contain duplicated urls'
    return 1
  fi
}

function check_for_misplaced_png_evidences {
  local misplaced_images

  find {code,hack}/*/*/*.{bmp,gif,jp*g,png,tiff} 2>/dev/null >&"${TEMP_FD}"
  mapfile -t misplaced_images <&"${TEMP_FD}"

  if test "${#misplaced_images[@]}" -eq 0
  then
    echo '[OK] Evidences are located under evidences/ folder'
    return 0
  else
    for file in "${misplaced_images[@]}"
    do
      echo "${file}"
    done
    echo "[ERROR] Evidences must be located under evidences/ folder"
    return 1
  fi
}

function check_for_non_png_evidences {
  local mime
  local path

  find hack/*/*/evidences/* | while read -r path
  do
    mime=$(file --mime-type --brief "${path}")
    if test "${mime}" != 'image/png'
    then
      echo "(${mime}) ${path}"
      echo "[ERROR] Evidences must have image/png mime type"
      return 1
    fi
  done
}

function check_lang_data {
  local target='./code/lang-data.yml'

  if pykwalify \
        --data-file "${target}" \
        --schema-file './code/lang-schema.yml' \
      &> "${TEMP_FILE}"
  then
    echo "[OK] ${target} complies with the schema"
    return 0
  else
    cat "${TEMP_FILE}"
    echo "[ERROR] ${target} does not comply with the schema"
    return 1
  fi
}

function check_mimes {
  local mime_encoding
  local mime_type
  local path
  local valid_mime_encodings=(
    'binary'
    'us-ascii'
    'utf-8'
  )
  local valid_mime_types=(
    'application/octet-stream'
    'image/png'
    'inode/x-empty'
    'text/html'
    'text/plain'
    'text/troff'
    'text/x-c'
    'text/x-c++'
    'text/x-java'
    'text/x-objective-c'
    'text/x-pascal'
    'text/x-perl'
    'text/x-php'
    'text/x-python'
    'text/x-ruby'
    'text/x-shellscript'
  )

  list_touched_files_in_last_commit | while read -r path
  do
    mime_encoding=$(file --brief --mime-encoding "${path}")
    mime_type=$(file --brief --mime-type "${path}")

    if is_elem_in_array "${mime_encoding}" "${valid_mime_encodings[@]}"
    then
      echo "[OK] Mime-encoding ${mime_encoding} at: ${path}"
    else
      echo "[ERROR] Mime-encoding ${mime_encoding} at: ${path}"
      echo '[INFO] Valid mime-encodings are:'
      display_array "${valid_mime_encodings[@]}"
      return 1
    fi

    if is_elem_in_array "${mime_type}" "${valid_mime_types[@]}"
    then
      echo "[OK] Mime-type ${mime_type} at: ${path}"
    else
      echo "[ERROR] Mime-type ${mime_type} at: ${path}"
      echo '[INFO] Valid mime-types are:'
      display_array "${valid_mime_types[@]}"
      return 1
    fi
  done
}

function check_site_data {
  local base
  local target
  local schema

  find ./code ./hack -mindepth 1 -maxdepth 1 -type d \
    | sort \
    | while read -r base
  do
    target="${base}/site-data.yml"
    schema="${base}/../site-schema.yml"

    if pykwalify \
          --data-file "${target}" \
          --schema-file "${schema}" \
        &> "${TEMP_FILE}"
    then
      echo "[OK] ${target} complies with the schema"
    else
      cat "${TEMP_FILE}"
      echo "[ERROR] ${target} does not comply with the schema"
      return 1
    fi
  done
}

function check_no_atfluid_account {
  if test "${IS_SOLUTION_COMMIT}" = "${TRUE}"
  then
    if list_touched_files_in_last_commit | grep 'atfluid'; then
      echo '[ERROR] Do not commit solutions with an *atfluid account'
      echo '        Use your personal GitLab account'
      return 1
    else
      echo '[OK] Author is using his/her personal GitLab account'
      return 0
    fi
  else
    echo '[SKIPPED] Not a solution commit'
    return 0
  fi
}

function check_status_code_of_urls_in_file {
  local file_name="${1}"
  local url
  local status_code

  list_touched_files_in_last_commit \
    | (grep "${file_name}" || cat) \
    | xargs -n 1 cat \
    | (grep -vP '^\s+$' || cat) \
    | while read -r url
  do
    status_code=$(curl -o /dev/null -s -w '%{http_code}' "${url}")
    if test "${status_code}" = '200'
    then
      echo "[OK] HTTP ${status_code} in ${file_name}, url: '${url}'"
    else
      echo "[ERROR] HTTP ${status_code} in ${file_name}, url: '${url}'"
      return 1
    fi
  done
}

function check_that_solution_and_commit_author_match {
  if test "${IS_SOLUTION_COMMIT}" = "${TRUE}"
  then
    if list_touched_files_in_last_commit | grep -qP "${GITLAB_USER_LOGIN}"
    then
      echo '[OK] Solution and GitLab username match'
      return 0
    else
      echo '[ERROR] Solution and GitLab username do not match'
      return 1
    fi
  else
    echo '[SKIPPED] Not a solution commit'
    return 0
  fi
}

function check_that_filenames_are_short {
  if find code | sed 's_^.*/__g' | grep -P '.{36,}'
  then
    echo '[ERROR] All filenames must be less than or equal to 36 characters'
    return 1
  else
    echo '[OK] Filenames are less than or equal to 36 characters'
    return 0
  fi
}

function check_that_github_urls_are_raw {
  if grep -r --include=OTHERS.lst --exclude-dir=.git github \
        code/4clojure \
        code/cod* \
        code/exercism \
        code/hackerearth \
        code/hackerrank \
        code/projecteuler \
        code/rosecode \
        code/solveet \
        code/spoj \
      | grep -v 'raw'
  then
    echo '[ERROR] GitHub URLs in programming challenges must be raw links'
    return 1
  else
    echo '[OK] GitHub URLs in programming challenges are raw links'
    return 0
  fi
}

function check_that_paths_contain_only_allowed_characters {
  local allowed_pattern='^[a-z0-9.\-/]*$'
  if find code hack \
      | sed -E 's/^.*(LINK|DATA|OTHERS).lst$//g' \
      | sed -E 's/^.*(SPEC).txt$//g' \
      | grep -vP "${allowed_pattern}"
  then
    echo "[ERROR] Paths contain characters outside of ${allowed_pattern}"
    return 1
  else
    echo "[OK] Paths contain characters only in ${allowed_pattern}"
    return 0
  fi
}

function check_that_there_are_no_tabs {
  # Go linter asks for tabs
  if grep -nrIP --exclude-dir=.git* --exclude=*.go '\t'
  then
    echo '[ERROR] Indentation with [TAB]'
    return 1
  else
    echo '[OK] Indentation with spaces'
    return 0
  fi
}

function check_that_there_is_not_asc_extension {
  if find code articles | grep '\.asc$'
  then
    echo '[ERROR] ASCIIDoc extensions must be .adoc, not .asc'
    return 1
  else
    echo '[OK] ASCIIDoc extensions are .adoc'
    return 0
  fi
}

function check_80_columns_margin {
  local path

  list_touched_files_in_last_commit | while read -r path
  do
    if grep \
          --binary-files=without-match \
          --exclude={.mailmap,*.lst} \
          --exclude-dir=.git \
          --perl-regex  \
          --quiet \
        '^.{81,}$' \
        "${path}"
    then
      echo '[ERROR] Wrap your code at column 80'
      echo "  ${path}"
      return 1
    else
      echo "[OK] Code is under 80 columns: ${path}"
    fi
  done
}

function display_array {
  local elem
  local array

  for elem in "$@";
  do
    echo "${elem}"
  done
}

function is_elem_in_array {
  local elem
  local array
  local string

  elem="${1}"
  shift 1
  array=("$@")

  for string in "${array[@]}";
  do
    if test "${string}" == "${elem}"
    then
      return 0
    fi
  done

  return 1
}

function lint_java {
  local path

  list_touched_files_in_last_commit \
    | (grep '\.java$' || cat) \
    | while read -r path
  do
    if java -jar java-checkstyle.jar -c java-checkstyle-config.xml "${path}" \
      | grep -P '(WARN|ERROR)'
    then
      echo "[ERROR] Java Linter: ${path}"
      return 1
    else
      echo "[OK] Java Linter: ${path}"
    fi
  done
}

function list_touched_files_in_last_commit {
  local path

  git show --format= --name-only HEAD | while read -r path
  do
    if test -e "${path}"
    then
      echo "${path}"
    fi
  done
}

function run_pre_commit {
  list_touched_files_in_last_commit | xargs pre-commit run -v --files
}
