pkgs:

{ pkgName
, requirement
}:

let
  pythonEnv = import ../python-env pkgs;
in
  pkgs.stdenv.mkDerivation rec {
    name = "python-package-${pkgName}";
    inherit requirement;
    srcGenericShellOptions = ../../include/generic/shell-options.sh;
    srcGenericDirStructure = ../../include/generic/dir-structure.sh;
    buildInputs = [ pythonEnv ];
    builder = ./builder.sh;
  }
