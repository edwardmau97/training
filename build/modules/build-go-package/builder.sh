# shellcheck shell=bash

source "${stdenv}/setup"
source "${srcGenericShellOptions}"
source "${srcGenericDirStructure}"

GOPATH="${PWD}/root/go" HOME="${PWD}/root/go" go get -u "${requirement}"

mkdir "${out}"
mv root/go/* "${out}"
